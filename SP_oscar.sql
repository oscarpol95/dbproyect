
-- -----------------------------------------------------
-- procedure new_ingreso_gastos
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_ingreso_gastos`(IN idgastos INT(11),
IN  fecha DATE,IN tipo_rubro VARCHAR(20) , IN detalle VARCHAR(30),
IN cantidad INT(11),IN costo_uni FLOAT, IN cargos_adi FLOAT, IN descuento FLOAT, 
IN cedula VARCHAR(10))
BEGIN
insert into gastos(idgastos, fecha,tipo_rubro, detalle) 
values (idgastos, fecha, tipo_rubro, detalle );

insert into desglose_gastos(
idgastos,cantidad,costo_unitario, subtotal, descuento, cargos_adicionales,total)
values(
(SELECT LAST_INSERT_ID()), cantidad, costo_uni, (cantidad*costo_uni), descuento, cargos_adi,
((cantidad*costo_uni)+ cargos_adi - descuento));


if cedula is not null then
	insert into gastopersonal(cedula,idgastos) values(cedula, (SELECT LAST_INSERT_ID()));
end if;

END$$

DELIMITER ;


-- procedure new_baja

CREATE DEFINER=`root`@`localhost` PROCEDURE `new_baja`(IN idbaja INT(11), 
IN fecha DATE, IN tipo VARCHAR(10), IN pesolb FLOAT, IN preciolb FLOAT, 
IN rp VARCHAR(3), IN detalle VARCHAR(30))
BEGIN



if tipo  like 'Muerte' then
	INSERT INTO bajas values(idbaja, fecha, tipo, 0, 0, rp, 0);
else
	INSERT INTO bajas values(idbaja, fecha, tipo, pesolb, preciolb, rp, 
    (pesolb*preciolb));
     INSERT INTO ingresos (id_bajas,detalle,fecha) VALUES ((SELECT LAST_INSERT_ID()),detalle,fecha);

end if;

update animal
set animal.estado = 'INACTIVO'
where animal.RP = rp;


END

-- fin procedure
-- procedure borrar_baja

CREATE DEFINER=`root`@`localhost` PROCEDURE `borrar_baja`(IN id_baja VARCHAR(3), IN rp VARCHAR(3))
BEGIN

	DELETE FROM bajas WHERE bajas.id_baja = id_baja;
	
    update animal
	set animal.estado = 'ACTIVO'
	where animal.RP = rp;

END

-- fin procedure

-- procedure update_baja

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_baja`(IN idbaja INT(11), 
IN fecha DATE, IN tipo VARCHAR(10), IN pesolb FLOAT, IN preciolb FLOAT, 
IN rp VARCHAR(3), IN detalle VARCHAR(30))
BEGIN

update bajas
set bajas.fecha_baja = fecha,  bajas.tipo_baja = tipo, bajas.peso_libra = pesolb, bajas.precio_por_libras = preciolb, bajas.rp_vaca = rp, 
bajas.total = (pesolb*preciolb)
where bajas.id_baja = idbaja;

update ingresos
set ingresos.detalle = detalle, ingresos.fecha = fecha
where ingresos.id_bajas = idbaja;

END

-- fin procedure. 