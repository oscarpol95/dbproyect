-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema discosdurostest
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema discosdurostest
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `discosdurostest` DEFAULT CHARACTER SET utf8 ;
USE `discosdurostest` ;

-- -----------------------------------------------------
-- Table `discosdurostest`.`persona`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`persona` (
  `cedula` VARCHAR(10) NOT NULL,
  `nombre` VARCHAR(30) NOT NULL,
  `direccion` VARCHAR(20) NOT NULL,
  `telefono_casa` VARCHAR(9) NULL DEFAULT NULL,
  `telefono_movil` VARCHAR(10) NULL DEFAULT NULL,
  `id_persona` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`cedula`),
  INDEX `auto` (`id_persona` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 13
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`area`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`area` (
  `codigo` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(30) NOT NULL,
  `direccion` VARCHAR(40) NOT NULL,
  `dimension` FLOAT NOT NULL,
  `capacidadMaxima` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`codigo`))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`animal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`animal` (
  `RP` VARCHAR(3) NOT NULL,
  `n_arete` VARCHAR(4) NULL DEFAULT NULL,
  `id_area` INT(11) NOT NULL,
  `nombre` VARCHAR(15) NULL DEFAULT NULL,
  `sexo` VARCHAR(10) NOT NULL,
  `fecha_llegada` DATE NULL DEFAULT NULL,
  `hacienda_origen` VARCHAR(15) NULL DEFAULT NULL,
  `propietario` VARCHAR(10) NOT NULL,
  `rp_madre` VARCHAR(3) NULL DEFAULT NULL,
  `rp_padre` VARCHAR(3) NULL DEFAULT NULL,
  `estado` VARCHAR(10) NOT NULL,
  `raza` VARCHAR(15) NULL DEFAULT NULL,
  `rgd` VARCHAR(10) NULL DEFAULT NULL,
  `rgd padre` VARCHAR(10) NULL DEFAULT NULL,
  PRIMARY KEY (`RP`),
  INDEX `animal_fk3_idx` (`rp_madre` ASC),
  INDEX `animal_fk4_idx` (`rp_padre` ASC),
  INDEX `animal_fk2_idx` (`id_area` ASC),
  INDEX `animal_fk1_idx` (`propietario` ASC),
  CONSTRAINT `animal_fk1`
    FOREIGN KEY (`propietario`)
    REFERENCES `discosdurostest`.`persona` (`cedula`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `animal_fk2`
    FOREIGN KEY (`id_area`)
    REFERENCES `discosdurostest`.`area` (`codigo`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `animal_fk3`
    FOREIGN KEY (`rp_madre`)
    REFERENCES `discosdurostest`.`animal` (`RP`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `animal_fk4`
    FOREIGN KEY (`rp_padre`)
    REFERENCES `discosdurostest`.`animal` (`RP`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`partos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`partos` (
  `rp_padre` VARCHAR(10) NOT NULL,
  `fecha_parto` DATE NOT NULL,
  `sexo` VARCHAR(10) NOT NULL,
  `estado` VARCHAR(10) NOT NULL,
  `id_parto` INT(11) NOT NULL AUTO_INCREMENT,
  `rp_vaca` VARCHAR(3) NOT NULL,
  PRIMARY KEY (`id_parto`),
  INDEX `fk1_idx` (`rp_vaca` ASC),
  CONSTRAINT `partos_fk1`
    FOREIGN KEY (`rp_vaca`)
    REFERENCES `discosdurostest`.`animal` (`RP`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`animal_parto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`animal_parto` (
  `RP` VARCHAR(3) NOT NULL,
  `id_parto` INT(11) NOT NULL,
  INDEX `animal_parto_fk1_idx` (`id_parto` ASC),
  INDEX `animal_parto_fk2_idx` (`RP` ASC),
  CONSTRAINT `animal_parto_fk1`
    FOREIGN KEY (`id_parto`)
    REFERENCES `discosdurostest`.`partos` (`id_parto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `animal_parto_fk2`
    FOREIGN KEY (`RP`)
    REFERENCES `discosdurostest`.`animal` (`RP`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`bajas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`bajas` (
  `id_baja` INT(11) NOT NULL AUTO_INCREMENT,
  `fecha_baja` DATE NOT NULL,
  `tipo_baja` VARCHAR(10) NOT NULL,
  `peso_libras` FLOAT NULL DEFAULT NULL,
  `precio_por_libras` FLOAT NULL DEFAULT NULL,
  `rp_vaca` VARCHAR(3) NOT NULL,
  `total` FLOAT NULL DEFAULT NULL,
  PRIMARY KEY (`id_baja`),
  UNIQUE INDEX `id_baja_UNIQUE` (`id_baja` ASC),
  INDEX `bajas_fk1_idx` (`rp_vaca` ASC),
  CONSTRAINT `bajas_fk1`
    FOREIGN KEY (`rp_vaca`)
    REFERENCES `discosdurostest`.`animal` (`RP`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`control_gestacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`control_gestacion` (
  `id_reproduccion` INT(5) NOT NULL,
  `fecha_concepcion` DATE NOT NULL COMMENT 'IATF',
  `meses_gestacion` INT(11) NOT NULL,
  `dias_gestacion` INT(11) NOT NULL,
  `fecha_estimada_parto` DATE NOT NULL,
  UNIQUE INDEX `id_reproduccion_UNIQUE` (`id_reproduccion` ASC),
  INDEX `control_gestacion_fk1_idx` (`id_reproduccion` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`control_partos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`control_partos` (
  `rp_animal` VARCHAR(3) NOT NULL,
  `meses_abiertos` INT(11) NOT NULL,
  `dias_abiertos` INT(11) NOT NULL,
  `fecha_ultima_parto` DATE NOT NULL,
  INDEX `control_partos_fk1_idx` (`rp_animal` ASC),
  CONSTRAINT `control_partos_fk1`
    FOREIGN KEY (`rp_animal`)
    REFERENCES `discosdurostest`.`animal` (`RP`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`gastos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`gastos` (
  `idgastos` INT(11) NOT NULL AUTO_INCREMENT,
  `fecha` DATE NULL DEFAULT NULL,
  `tipo_rubro` VARCHAR(10) NULL DEFAULT NULL,
  `detalle` VARCHAR(30) NULL DEFAULT NULL,
  PRIMARY KEY (`idgastos`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`desglose_gastos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`desglose_gastos` (
  `idgastos` INT(11) NOT NULL,
  `cantidad` INT(11) NOT NULL,
  `costo_unitario` FLOAT NOT NULL,
  `subtotal` FLOAT NOT NULL,
  `descuento` FLOAT NOT NULL,
  `cargos_adicionales` FLOAT NULL DEFAULT NULL,
  `total` FLOAT NOT NULL,
  INDEX `fk1_idx` (`idgastos` ASC),
  CONSTRAINT `fk1`
    FOREIGN KEY (`idgastos`)
    REFERENCES `discosdurostest`.`gastos` (`idgastos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`edades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`edades` (
  `RP` VARCHAR(3) NOT NULL,
  `fecha_nacimiento` DATE NOT NULL,
  `edades_años` INT(11) NOT NULL,
  `edades_meses` INT(11) NOT NULL,
  `edades_dias` INT(11) NOT NULL,
  `clasificacion` VARCHAR(10) NOT NULL,
  INDEX `edades_fk1_idx` (`RP` ASC),
  CONSTRAINT `edades_fk1`
    FOREIGN KEY (`RP`)
    REFERENCES `discosdurostest`.`animal` (`RP`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`gastopersonal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`gastopersonal` (
  `cedula` VARCHAR(10) NOT NULL,
  `idgastos` INT(11) NOT NULL,
  INDEX `idgastos_idx` (`idgastos` ASC),
  INDEX `ced_idx` (`cedula` ASC),
  CONSTRAINT `cedula`
    FOREIGN KEY (`cedula`)
    REFERENCES `discosdurostest`.`persona` (`cedula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idgastos`
    FOREIGN KEY (`idgastos`)
    REFERENCES `discosdurostest`.`gastos` (`idgastos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`hacienda`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`hacienda` (
  `RUC` VARCHAR(13) NOT NULL,
  `nombre_HAC` VARCHAR(20) NOT NULL,
  `ci_prop` VARCHAR(10) NOT NULL,
  `email` VARCHAR(30) NULL DEFAULT NULL,
  `id_hacienda` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_hacienda`),
  INDEX `auto` (`RUC` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`ingresos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`ingresos` (
  `id_ingresos` VARCHAR(3) NOT NULL,
  `id_bajas` INT(11) NOT NULL,
  `detalle` VARCHAR(30) NULL DEFAULT NULL,
  `fecha` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`id_ingresos`),
  UNIQUE INDEX `id_ingresos_UNIQUE` (`id_ingresos` ASC),
  INDEX `ingresos_fk1_idx` (`id_bajas` ASC),
  CONSTRAINT `ingresos_fk1`
    FOREIGN KEY (`id_bajas`)
    REFERENCES `discosdurostest`.`bajas` (`id_baja`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`medicamento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`medicamento` (
  `nombre` VARCHAR(20) NOT NULL,
  `tipo` VARCHAR(10) NOT NULL,
  `periodicidad` VARCHAR(10) NOT NULL,
  `posologia` VARCHAR(20) NOT NULL,
  `id_medicamento` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`nombre`),
  INDEX `auto` (`id_medicamento` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`pesaje`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`pesaje` (
  `idpesaje` INT(11) NOT NULL AUTO_INCREMENT,
  `rp_vaca` VARCHAR(3) NOT NULL,
  `peso` FLOAT NOT NULL,
  `fecha_pesaje` DATE NOT NULL,
  PRIMARY KEY (`idpesaje`),
  INDEX `pesaje_fk1_idx` (`rp_vaca` ASC),
  CONSTRAINT `pesaje_fk1`
    FOREIGN KEY (`rp_vaca`)
    REFERENCES `discosdurostest`.`animal` (`RP`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 14
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`plan_reproductivo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`plan_reproductivo` (
  `id_reproduccion` INT(5) NOT NULL AUTO_INCREMENT,
  `rp_padre` VARCHAR(10) NOT NULL,
  `rp_vaca` VARCHAR(3) NOT NULL,
  `aplica_dispositivo` BIT(1) NOT NULL,
  `condicion_corporal` FLOAT NOT NULL,
  `palpacion` BIT(1) NOT NULL,
  `comentario` VARCHAR(100) NULL DEFAULT NULL,
  `tipo_reproduccion` VARCHAR(20) NOT NULL,
  `fecha_plan` DATE NOT NULL,
  PRIMARY KEY (`id_reproduccion`),
  INDEX `plan_reproductivo_fk1_idx` (`rp_vaca` ASC),
  CONSTRAINT `plan_reproductivo_fk1`
    FOREIGN KEY (`rp_vaca`)
    REFERENCES `discosdurostest`.`animal` (`RP`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 22
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `discosdurostest`.`plan_sanitario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`plan_sanitario` (
  `id_plan` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(20) NOT NULL,
  `tipo` VARCHAR(10) NOT NULL,
  `fecha_aplicacion` DATE NOT NULL,
  `rp_vaca` VARCHAR(3) NOT NULL,
  `nombre_med` VARCHAR(20) NOT NULL,
  `dosis` FLOAT NOT NULL,
  PRIMARY KEY (`id_plan`),
  INDEX `plan_sanitario_fk1_idx` (`rp_vaca` ASC),
  INDEX `plan_sanitario_fk2_idx` (`nombre_med` ASC),
  CONSTRAINT `plan_sanitario_fk1`
    FOREIGN KEY (`rp_vaca`)
    REFERENCES `discosdurostest`.`animal` (`RP`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `plan_sanitario_fk2`
    FOREIGN KEY (`nombre_med`)
    REFERENCES `discosdurostest`.`medicamento` (`nombre`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 16
DEFAULT CHARACTER SET = utf8;

USE `discosdurostest` ;

-- -----------------------------------------------------
-- Placeholder table for view `discosdurostest`.`bajas_view`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`bajas_view` (`id_baja` INT, `rp_vaca` INT, `fecha_baja` INT, `tipo_baja` INT, `peso_libras` INT, `precio_por_libras` INT);

-- -----------------------------------------------------
-- Placeholder table for view `discosdurostest`.`numeros_parto_por_animal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `discosdurostest`.`numeros_parto_por_animal` (`id` INT);

-- -----------------------------------------------------
-- procedure borrar_animal
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `borrar_animal`(IN inRP VARCHAR(3))
BEGIN

	DELETE FROM animal WHERE RP = inRP;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure borrar_area
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `borrar_area`(IN nom VARCHAR(30))
BEGIN

Delete From area where nombre=nom;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure borrar_baja
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `borrar_baja`(IN id_baja VARCHAR(3))
BEGIN

	DELETE FROM bajas WHERE bajas.id_baja = id_baja;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure borrar_persona
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `borrar_persona`(in ced varchar(10))
BEGIN

Delete From persona where cedula=ced;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure borrar_planRepro
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `borrar_planRepro`(IN idPlan INT(11))
BEGIN

Delete From plan_reproductivo where plan_reproductivo.id_reproduccion = idPlan;

END$$

DELIMITER ;

-- -----------------------------------------------------
<<<<<<< HEAD
-- procedure borrar_planSani
=======
-- procedure buscar_animal
>>>>>>> 2eb9a37c5628868137b514de958adcca3ea74aea
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
<<<<<<< HEAD
CREATE DEFINER=`root`@`localhost` PROCEDURE `borrar_planSani`(IN id_planSani INT(11))
BEGIN

Delete From plan_sanitario where plan_sanitario.id_plan = id_planSani;

END$$
=======
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_animal`()
BEGIN
select x.rp,a.nombre,p.nombre,x.nombre,x.sexo,e.fecha_nacimiento,x.num_partos,w.estaPrenada,y.ultimoPesaje,z.ultimoPlanSani  from
(select a.*,num_hijos(a.sexo,count(p.rp_vaca))as num_partos
from partos p right  join animal a on a.rp=p.rp_vaca
group by a.rp)x ,persona p,area a,edades e,
(select noTiene_fecha(y.fecha_aplicacion) as ultimoPlanSani,y.rp
from(select p.fecha_aplicacion,a.rp from plan_sanitario p right  join animal a on a.rp=p.rp_vaca
order by p.fecha_aplicacion desc)y
group by (y.rp))z,
(select noTiene_fecha(y.fecha_pesaje) as ultimoPesaje,y.rp
from(select p.fecha_pesaje,a.rp from pesaje p right  join animal a on a.rp=p.rp_vaca
order by p.fecha_pesaje desc)y
group by (y.rp))y,
(select si_no(a.palpacion,b.sexo) as estaPrenada,b.rp from
(select 
s.rp_vaca,s.palpacion,s.fecha_plan,t.fecha_parto
from(

(
select  * from 
(select * from plan_reproductivo p  order by p.fecha_plan desc)x 
group by x.rp_vaca

)s,
>>>>>>> 2eb9a37c5628868137b514de958adcca3ea74aea

DELIMITER ;

<<<<<<< HEAD
-- -----------------------------------------------------
-- procedure buscar_animal
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_animal`()
BEGIN
select x.rp,a.nombre,p.nombre,x.nombre,x.sexo,e.fecha_nacimiento,x.num_partos,w.estaPrenada,y.ultimoPesaje,z.ultimoPlanSani  from
(select a.*,num_hijos(a.sexo,count(p.rp_vaca))as num_partos
from partos p right  join animal a on a.rp=p.rp_vaca
group by a.rp)x ,persona p,area a,edades e,
(select noTiene_fecha(y.fecha_aplicacion) as ultimoPlanSani,y.rp
from(select p.fecha_aplicacion,a.rp from plan_sanitario p right  join animal a on a.rp=p.rp_vaca
order by p.fecha_aplicacion desc)y
group by (y.rp))z,
(select noTiene_fecha(y.fecha_pesaje) as ultimoPesaje,y.rp
from(select p.fecha_pesaje,a.rp from pesaje p right  join animal a on a.rp=p.rp_vaca
order by p.fecha_pesaje desc)y
group by (y.rp))y,
(select si_no(a.palpacion,b.sexo) as estaPrenada,b.rp from
(select 
s.rp_vaca,s.palpacion,s.fecha_plan,t.fecha_parto
from(

(
select  * from 
(select * from plan_reproductivo p  order by p.fecha_plan desc)x 
group by x.rp_vaca

)s,



( 
select * from 
(select * from partos    order by fecha_parto  desc)y 
group by  y.rp_vaca 
)t
)
where
t.rp_vaca=s.rp_vaca and (month(t.fecha_parto)-month(s.fecha_plan)<10))a right  join animal b on b.rp=a.rp_vaca

)w
=======

( 
select * from 
(select * from partos    order by fecha_parto  desc)y 
group by  y.rp_vaca 
)t
)
where
t.rp_vaca=s.rp_vaca and (month(t.fecha_parto)-month(s.fecha_plan)<10))a right  join animal b on b.rp=a.rp_vaca

)w



where y.rp=x.rp and z.rp=x.rp and w.rp=x.rp and e.rp=x.rp

and x.propietario=p.cedula and a.codigo=x.id_area

order by y.rp asc;


>>>>>>> 2eb9a37c5628868137b514de958adcca3ea74aea



<<<<<<< HEAD
where y.rp=x.rp and z.rp=x.rp and w.rp=x.rp and e.rp=x.rp

and x.propietario=p.cedula and a.codigo=x.id_area

order by y.rp asc;



END$$
=======
-- -----------------------------------------------------
-- procedure buscar_animal_area
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_animal_area`(IN clave VARCHAR(50))
BEGIN

select x.rp,a.nombre,p.nombre,x.nombre,x.sexo,e.fecha_nacimiento,x.num_partos,w.estaPrenada,y.ultimoPesaje,z.ultimoPlanSani  from
>>>>>>> 2eb9a37c5628868137b514de958adcca3ea74aea

(select a.*,num_hijos(a.sexo,count(p.rp_vaca))as num_partos

<<<<<<< HEAD
-- -----------------------------------------------------
-- procedure buscar_animal_area
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_animal_area`(IN clave VARCHAR(50))
BEGIN

select x.rp,a.nombre,p.nombre,x.nombre,x.sexo,e.fecha_nacimiento,x.num_partos,w.estaPrenada,y.ultimoPesaje,z.ultimoPlanSani  from

(select a.*,num_hijos(a.sexo,count(p.rp_vaca))as num_partos

from partos p right  join animal a on a.rp=p.rp_vaca

group by a.rp)x ,area a,persona p,edades e,

(select noTiene_fecha(y.fecha_aplicacion) as ultimoPlanSani,y.rp

from(select p.fecha_aplicacion,a.rp from plan_sanitario p right  join animal a on a.rp=p.rp_vaca

order by p.fecha_aplicacion desc)y

group by (y.rp))z,

(select noTiene_fecha(y.fecha_pesaje) as ultimoPesaje,y.rp

from(select p.fecha_pesaje,a.rp from pesaje p right  join animal a on a.rp=p.rp_vaca

order by p.fecha_pesaje desc)y

group by (y.rp))y,

(select si_no(a.palpacion,b.sexo) as estaPrenada,b.rp from
(select 
s.rp_vaca,s.palpacion,s.fecha_plan,t.fecha_parto
from(

(
select  * from 
(select * from plan_reproductivo p  order by p.fecha_plan desc)x 
group by x.rp_vaca

)s,
=======
from partos p right  join animal a on a.rp=p.rp_vaca

group by a.rp)x ,area a,persona p,edades e,

(select noTiene_fecha(y.fecha_aplicacion) as ultimoPlanSani,y.rp

from(select p.fecha_aplicacion,a.rp from plan_sanitario p right  join animal a on a.rp=p.rp_vaca

order by p.fecha_aplicacion desc)y

group by (y.rp))z,

(select noTiene_fecha(y.fecha_pesaje) as ultimoPesaje,y.rp

from(select p.fecha_pesaje,a.rp from pesaje p right  join animal a on a.rp=p.rp_vaca

order by p.fecha_pesaje desc)y

group by (y.rp))y,

(select si_no(a.palpacion,b.sexo) as estaPrenada,b.rp from
(select 
s.rp_vaca,s.palpacion,s.fecha_plan,t.fecha_parto
from(

(
select  * from 
(select * from plan_reproductivo p  order by p.fecha_plan desc)x 
group by x.rp_vaca

)s,



( 
select * from 
(select * from partos    order by fecha_parto  desc)y 
group by  y.rp_vaca 
)t
)
where
t.rp_vaca=s.rp_vaca and (month(t.fecha_parto)-month(s.fecha_plan)<10))a right  join animal b on b.rp=a.rp_vaca
>>>>>>> 2eb9a37c5628868137b514de958adcca3ea74aea

)w

<<<<<<< HEAD

( 
select * from 
(select * from partos    order by fecha_parto  desc)y 
group by  y.rp_vaca 
)t
)
where
t.rp_vaca=s.rp_vaca and (month(t.fecha_parto)-month(s.fecha_plan)<10))a right  join animal b on b.rp=a.rp_vaca

)w

where y.rp=x.rp and z.rp=x.rp and w.rp=x.rp and  e.rp=x.rp

and x.id_area = a.codigo and a.nombre LIKE concat('%',clave,'%') and x.propietario=p.cedula

=======
where y.rp=x.rp and z.rp=x.rp and w.rp=x.rp and  e.rp=x.rp

and x.id_area = a.codigo and a.nombre LIKE concat('%',clave,'%') and x.propietario=p.cedula

>>>>>>> 2eb9a37c5628868137b514de958adcca3ea74aea
order by y.rp asc;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_animal_info
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_animal_info`(IN RP_in VARCHAR(3))
BEGIN

SELECT n_arete, rgd, raza, clasificacion, fecha_llegada, hacienda_origen, edades_años, edades_meses
FROM animal, area, edades 
WHERE RP_in = animal.RP AND animal.id_area = area.codigo AND animal.RP = edades.RP;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_animal_nombre
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_animal_nombre`(IN clave VARCHAR(50))
BEGIN

select x.rp,a.nombre,p.nombre,x.nombre,x.sexo,e.fecha_nacimiento,x.num_partos,w.estaPrenada,y.ultimoPesaje,z.ultimoPlanSani  from

(select a.*,num_hijos(a.sexo,count(p.rp_vaca))as num_partos

from partos p right  join animal a on a.rp=p.rp_vaca

group by a.rp)x ,area a,persona p,edades e,

(select noTiene_fecha(y.fecha_aplicacion) as ultimoPlanSani,y.rp

from(select p.fecha_aplicacion,a.rp from plan_sanitario p right  join animal a on a.rp=p.rp_vaca

order by p.fecha_aplicacion desc)y

group by (y.rp))z,

(select noTiene_fecha(y.fecha_pesaje) as ultimoPesaje,y.rp

from(select p.fecha_pesaje,a.rp from pesaje p right  join animal a on a.rp=p.rp_vaca

order by p.fecha_pesaje desc)y

group by (y.rp))y,

(select si_no(a.palpacion,b.sexo) as estaPrenada,b.rp from
(select 
s.rp_vaca,s.palpacion,s.fecha_plan,t.fecha_parto
from(

(
select  * from 
(select * from plan_reproductivo p  order by p.fecha_plan desc)x 
group by x.rp_vaca
<<<<<<< HEAD
=======

)s,



( 
select * from 
(select * from partos    order by fecha_parto  desc)y 
group by  y.rp_vaca 
)t
)
where
t.rp_vaca=s.rp_vaca and (month(t.fecha_parto)-month(s.fecha_plan)<10))a right  join animal b on b.rp=a.rp_vaca

)w

where y.rp=x.rp and z.rp=x.rp and w.rp=x.rp 

and

 x.nombre LIKE concat('%',clave,'%') and x.propietario=p.cedula and x.id_area = a.codigo and e.rp=x.rp

order by y.rp asc;
>>>>>>> 2eb9a37c5628868137b514de958adcca3ea74aea

)s,


<<<<<<< HEAD

( 
select * from 
(select * from partos    order by fecha_parto  desc)y 
group by  y.rp_vaca 
)t
)
where
t.rp_vaca=s.rp_vaca and (month(t.fecha_parto)-month(s.fecha_plan)<10))a right  join animal b on b.rp=a.rp_vaca

)w

where y.rp=x.rp and z.rp=x.rp and w.rp=x.rp 

and

 x.nombre LIKE concat('%',clave,'%') and x.propietario=p.cedula and x.id_area = a.codigo and e.rp=x.rp

order by y.rp asc;

END$$
=======
-- -----------------------------------------------------
-- procedure buscar_animal_propietario
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_animal_propietario`(IN clave VARCHAR(50))
BEGIN
>>>>>>> 2eb9a37c5628868137b514de958adcca3ea74aea

select x.rp,a.nombre,p.nombre,x.nombre,x.sexo,e.fecha_nacimiento,x.num_partos,w.estaPrenada,y.ultimoPesaje,z.ultimoPlanSani  from

<<<<<<< HEAD
-- -----------------------------------------------------
-- procedure buscar_animal_propietario
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_animal_propietario`(IN clave VARCHAR(50))
BEGIN

select x.rp,a.nombre,p.nombre,x.nombre,x.sexo,e.fecha_nacimiento,x.num_partos,w.estaPrenada,y.ultimoPesaje,z.ultimoPlanSani  from

(select a.*,num_hijos(a.sexo,count(p.rp_vaca)) as num_partos

from partos p right  join animal a on a.rp=p.rp_vaca

group by a.rp)x ,edades e,
=======
(select a.*,num_hijos(a.sexo,count(p.rp_vaca)) as num_partos

from partos p right  join animal a on a.rp=p.rp_vaca

group by a.rp)x ,edades e,

(select noTiene_fecha(y.fecha_aplicacion) as ultimoPlanSani,y.rp

from(select p.fecha_aplicacion,a.rp from plan_sanitario p right  join animal a on a.rp=p.rp_vaca

order by p.fecha_aplicacion desc)y

group by (y.rp))z,

(select noTiene_fecha(y.fecha_pesaje) as ultimoPesaje,y.rp

from(select p.fecha_pesaje,a.rp from pesaje p right  join animal a on a.rp=p.rp_vaca

order by p.fecha_pesaje desc)y

group by (y.rp))y,persona p,area a,

(select si_no(a.palpacion,b.sexo) as estaPrenada,b.rp from
(select 
s.rp_vaca,s.palpacion,s.fecha_plan,t.fecha_parto
from(

(
select  * from 
(select * from plan_reproductivo p  order by p.fecha_plan desc)x 
group by x.rp_vaca

)s,



( 
select * from 
(select * from partos    order by fecha_parto  desc)y 
group by  y.rp_vaca 
)t
)
where
t.rp_vaca=s.rp_vaca and (month(t.fecha_parto)-month(s.fecha_plan)<10))a right  join animal b on b.rp=a.rp_vaca

)w

where y.rp=x.rp and z.rp=x.rp and w.rp=x.rp 

and x.propietario = p.cedula and p.nombre LIKE concat('%',clave,'%') and x.id_area = a.codigo and  e.rp=x.rp

order by y.rp asc;


>>>>>>> 2eb9a37c5628868137b514de958adcca3ea74aea

(select noTiene_fecha(y.fecha_aplicacion) as ultimoPlanSani,y.rp

from(select p.fecha_aplicacion,a.rp from plan_sanitario p right  join animal a on a.rp=p.rp_vaca

<<<<<<< HEAD
order by p.fecha_aplicacion desc)y

group by (y.rp))z,

(select noTiene_fecha(y.fecha_pesaje) as ultimoPesaje,y.rp

from(select p.fecha_pesaje,a.rp from pesaje p right  join animal a on a.rp=p.rp_vaca

order by p.fecha_pesaje desc)y

group by (y.rp))y,persona p,area a,

(select si_no(a.palpacion,b.sexo) as estaPrenada,b.rp from
(select 
s.rp_vaca,s.palpacion,s.fecha_plan,t.fecha_parto
from(

(
select  * from 
(select * from plan_reproductivo p  order by p.fecha_plan desc)x 
group by x.rp_vaca

)s,



( 
select * from 
(select * from partos    order by fecha_parto  desc)y 
group by  y.rp_vaca 
)t
)
where
t.rp_vaca=s.rp_vaca and (month(t.fecha_parto)-month(s.fecha_plan)<10))a right  join animal b on b.rp=a.rp_vaca

)w

where y.rp=x.rp and z.rp=x.rp and w.rp=x.rp 

and x.propietario = p.cedula and p.nombre LIKE concat('%',clave,'%') and x.id_area = a.codigo and  e.rp=x.rp

order by y.rp asc;


=======
-- -----------------------------------------------------
-- procedure buscar_animal_rp
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_animal_rp`(IN clave VARCHAR(50))
BEGIN

select x.rp,a.nombre,p.nombre,x.nombre,x.sexo,e.fecha_nacimiento,x.num_partos,w.estaPrenada,y.ultimoPesaje,z.ultimoPlanSani  from

(select a.*,num_hijos(a.sexo,count(p.rp_vaca))as num_partos

from partos p right  join animal a on a.rp=p.rp_vaca

group by a.rp)x ,edades e,

(select noTiene_fecha(y.fecha_aplicacion) as ultimoPlanSani,y.rp

from(select p.fecha_aplicacion,a.rp from plan_sanitario p right  join animal a on a.rp=p.rp_vaca

order by p.fecha_aplicacion desc)y

group by (y.rp))z,

(select noTiene_fecha(y.fecha_pesaje) as ultimoPesaje,y.rp

from(select p.fecha_pesaje,a.rp from pesaje p right  join animal a on a.rp=p.rp_vaca

order by p.fecha_pesaje desc)y

group by (y.rp))y,persona p,area a,

(select si_no(a.palpacion,b.sexo) as estaPrenada,b.rp from
(select 
s.rp_vaca,s.palpacion,s.fecha_plan,t.fecha_parto
from(

(
select  * from 
(select * from plan_reproductivo p  order by p.fecha_plan desc)x 
group by x.rp_vaca

)s,
>>>>>>> 2eb9a37c5628868137b514de958adcca3ea74aea



<<<<<<< HEAD
-- -----------------------------------------------------
-- procedure buscar_animal_rp
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_animal_rp`(IN clave VARCHAR(50))
BEGIN

select x.rp,a.nombre,p.nombre,x.nombre,x.sexo,e.fecha_nacimiento,x.num_partos,w.estaPrenada,y.ultimoPesaje,z.ultimoPlanSani  from

(select a.*,num_hijos(a.sexo,count(p.rp_vaca))as num_partos

from partos p right  join animal a on a.rp=p.rp_vaca

group by a.rp)x ,edades e,

(select noTiene_fecha(y.fecha_aplicacion) as ultimoPlanSani,y.rp

from(select p.fecha_aplicacion,a.rp from plan_sanitario p right  join animal a on a.rp=p.rp_vaca

order by p.fecha_aplicacion desc)y

group by (y.rp))z,

(select noTiene_fecha(y.fecha_pesaje) as ultimoPesaje,y.rp

from(select p.fecha_pesaje,a.rp from pesaje p right  join animal a on a.rp=p.rp_vaca

order by p.fecha_pesaje desc)y

group by (y.rp))y,persona p,area a,

(select si_no(a.palpacion,b.sexo) as estaPrenada,b.rp from
(select 
s.rp_vaca,s.palpacion,s.fecha_plan,t.fecha_parto
from(

(
select  * from 
(select * from plan_reproductivo p  order by p.fecha_plan desc)x 
group by x.rp_vaca

)s,



( 
select * from 
(select * from partos    order by fecha_parto  desc)y 
group by  y.rp_vaca 
)t
)
where
t.rp_vaca=s.rp_vaca and (month(t.fecha_parto)-month(s.fecha_plan)<10))a right  join animal b on b.rp=a.rp_vaca

)w

where y.rp=x.rp and z.rp=x.rp and w.rp=x.rp and x.propietario = p.cedula and x.id_area = a.codigo

=======
( 
select * from 
(select * from partos    order by fecha_parto  desc)y 
group by  y.rp_vaca 
)t
)
where
t.rp_vaca=s.rp_vaca and (month(t.fecha_parto)-month(s.fecha_plan)<10))a right  join animal b on b.rp=a.rp_vaca

)w

where y.rp=x.rp and z.rp=x.rp and w.rp=x.rp and x.propietario = p.cedula and x.id_area = a.codigo

>>>>>>> 2eb9a37c5628868137b514de958adcca3ea74aea
and x.RP LIKE concat('%',clave,'%') and e.rp=x.rp

order by y.rp asc;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_area
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_area`()
BEGIN

select * from area;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_areaNombre
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_areaNombre`(IN nom VARCHAR(30))
BEGIN

select * from area where nombre=nom;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_bajas
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_bajas`()
BEGIN

select * from bajas;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_cont_ges
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_cont_ges`(in rp varchar(3))
BEGIN
select a.rp_vaca,si_no_dgvplanrepro(a.palpacion),(a.meses_gestacion),(a.dias_gestacion)  from
(select 
s.rp_vaca,s.palpacion,s.fecha_plan,t.fecha_parto,s.meses_gestacion,s.dias_gestacion
from((select  * from 
(select p.id_reproduccion,p.rp_vaca,p.palpacion,p.fecha_plan,c.meses_gestacion,c.dias_gestacion from plan_reproductivo p left join control_gestacion c  on c.id_reproduccion=p.id_reproduccion order by p.fecha_plan desc)x 
group by x.rp_vaca)s,
( select * from 
(select * from partos    order by fecha_parto  desc)y group by  y.rp_vaca )t)where
t.rp_vaca=s.rp_vaca and (month(t.fecha_parto)-month(s.fecha_plan)<10))a
where a.rp_vaca=rp ;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_gastosTotal
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_gastosTotal`()
BEGIN

select gastos.fecha, gastos.tipo_rubro, gastos.detalle, desglose_gastos.cantidad, desglose_gastos.costo_unitario, desglose_gastos.subtotal,

desglose_gastos.cargos_adicionales, desglose_gastos.descuento, desglose_gastos.total

from gastos, desglose_gastos

where gastos.idgastos = desglose_gastos.idgastos;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_hacienda
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_hacienda`()
BEGIN
select * from hacienda;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_ingreso
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_ingreso`()
BEGIN



select ingresos.fecha, bajas.rp_vaca, bajas.peso_libras, bajas.peso_libras, bajas.total,  ingresos.detalle

from bajas, ingresos

where bajas.id_baja = ingresos.id_bajas;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_medicamento
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_medicamento`()
BEGIN

SELECT * from medicamento;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_parto
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_parto`()
BEGIN

select (select count(*) from partos) - count(a.rp)

from partos p , animal a

where  a.rp!=p.rp_vaca and a.sexo='hembra'

group by a.rp;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_personNombre
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_personNombre`(IN nom VARCHAR(30))
BEGIN

select * from persona where nombre=nom;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_persona
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_persona`()
BEGIN

select * from persona;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_pesaje
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_pesaje`(IN desde DATE, IN hasta DATE, IN rp VARCHAR(3))
BEGIN

SELECT * FROM pesaje WHERE (pesaje.fecha_pesaje>=desde) AND (pesaje.fecha_pesaje<=hasta) AND pesaje.rp_vaca =rp ;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_pla_repro_individual
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_pla_repro_individual`(IN cod INT(11))
BEGIN
select * from plan_reproductivo where plan_reproductivo.id_reproduccion  = cod ;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_plan_repro
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_plan_repro`(in rp varchar(3))
BEGIN
select id_reproduccion,rp_padre,fecha_plan,si_no_dgvplanrepro(aplica_dispositivo),condicion_corporal,comentario,
tipo_reproduccion,si_no_dgvplanrepro(palpacion)
from plan_reproductivo
where rp_vaca=rp;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_plan_repro_fecha
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_plan_repro_fecha`(in rp varchar(3),IN fecha_desde timestamp,IN fecha_hasta timestamp)
BEGIN
select id_reproduccion,rp_padre,fecha_plan,si_no_dgvplanrepro(aplica_dispositivo),condicion_corporal,comentario,
tipo_reproduccion,si_no_dgvplanrepro(palpacion)
from plan_reproductivo
where (plan_reproductivo.fecha_plan >= fecha_desde  and plan_reproductivo.fecha_plan <=fecha_hasta) and plan_reproductivo.rp_vaca LIKE concat('%',rp,'%');
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_plan_sani_por_fecha
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_plan_sani_por_fecha`(in rp varchar(3),IN fecha_desde DATE,IN fecha_hasta DATE)
BEGIN
<<<<<<< HEAD
select plan_sanitario.id_plan, plan_sanitario.nombre, plan_sanitario.fecha_aplicacion, 
plan_sanitario.tipo,plan_sanitario.nombre_med, medicamento.posologia, medicamento.periodicidad,
 plan_sanitario.dosis
from medicamento, plan_sanitario
where medicamento.nombre = plan_sanitario.nombre_med and 
(plan_sanitario.fecha_aplicacion between fecha_desde  and fecha_hasta) and plan_sanitario.rp_vaca 
LIKE concat('%',rp,'%');
=======
select plan_sanitario.id_plan, plan_sanitario.nombre, plan_sanitario.fecha_aplicacion, plan_sanitario.tipo,
plan_sanitario.nombre_med, medicamento.posologia, medicamento.periodicidad, plan_sanitario.dosis, pesaje.peso
from medicamento, plan_sanitario, pesaje
where medicamento.nombre = plan_sanitario.nombre_med and plan_sanitario.rp_vaca = pesaje.rp_vaca and (plan_sanitario.fecha_aplicacion between fecha_desde  and fecha_hasta) and plan_sanitario.rp_vaca LIKE concat('%',rp,'%');
>>>>>>> 2eb9a37c5628868137b514de958adcca3ea74aea
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_plan_sanitario
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_plan_sanitario`(in rp varchar(3)
)
BEGIN
select plan_sanitario.id_plan, plan_sanitario.nombre, plan_sanitario.fecha_aplicacion, plan_sanitario.tipo,
<<<<<<< HEAD
plan_sanitario.nombre_med, medicamento.posologia, medicamento.periodicidad, plan_sanitario.dosis
from medicamento, plan_sanitario
where medicamento.nombre = plan_sanitario.nombre_med and plan_sanitario.rp_vaca LIKE concat('%',rp,'%')
=======
plan_sanitario.nombre_med, medicamento.posologia, medicamento.periodicidad, plan_sanitario.dosis, pesaje.peso
from medicamento, plan_sanitario, pesaje
where medicamento.nombre = plan_sanitario.nombre_med  and plan_sanitario.rp_vaca = pesaje.rp_vaca and plan_sanitario.rp_vaca LIKE concat('%',rp,'%')
>>>>>>> 2eb9a37c5628868137b514de958adcca3ea74aea
;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buscar_plan_sanitario_tabla
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_plan_sanitario_tabla`(IN cod INT(11))
BEGIN

select * from plan_sanitario where plan_sanitario.id_plan  = cod ;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- function clasificacionPorEdad
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `clasificacionPorEdad`(anios int,meses int) RETURNS varchar(20) CHARSET utf8
BEGIN

	declare clasi varchar(20);

	if (anios >= 3 and meses >= 6)  or(anios >=4 and meses >= 0)THEN

				RETURN 'Vaca';

	

    elseif ((anios >= 2 and anios <= 3 ) and ( meses >= 0)) THEN

				RETURN 'Vacona';

	

    elseif ((anios >= 0 and anios <= 1 ) and (meses >= 9)) or (anios=1 and meses>=0) THEN

				RETURN 'Vaca Levante';

	elseif(anios = 0 and meses < 9) THEN

				RETURN 'Ternera';

    

    end if;

      

        

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure conteoAnimalesArea
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `conteoAnimalesArea`(IN inArea VARCHAR(30))
BEGIN

	select count(*) from area, animal

	where area.codigo = animal.id_area and area.nombre = inArea;

END$$

DELIMITER ;

-- -----------------------------------------------------
<<<<<<< HEAD
-- procedure conteoAnimalesPersona
=======
-- procedure new_animal
>>>>>>> 2eb9a37c5628868137b514de958adcca3ea74aea
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
<<<<<<< HEAD
CREATE DEFINER=`root`@`localhost` PROCEDURE `conteoAnimalesPersona`(IN inPersona VARCHAR(30))
BEGIN

	select count(*) from animal

	where animal.propietario = inPersona;
=======
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_animal`(IN rp VARCHAR(3),
IN n_arete VARCHAR(4),IN id_area VARCHAR(30),IN nom VARCHAR(15),
IN sexo VARCHAR(10),IN fecha_llega DATE,IN haci_ori VARCHAR(15),
IN propi VARCHAR(30),IN rp_madre VARCHAR(3),IN rp_padre VARCHAR(3),
IN estado VARCHAR(10),IN raza VARCHAR(15),IN rgd VARCHAR(15),IN rgd_padre VARCHAR(15))
BEGIN

INSERT INTO animal values(rp,n_arete,
(SELECT codigo FROM area  WHERE nombre = id_area),nom,sexo,fecha_llega,haci_ori,
(SELECT cedula FROM persona  WHERE nombre = propi),
(select a.rp from animal a where a.rp=rp_madre),
(select a.rp from animal a where a.rp=rp_padre),estado,raza,rgd,rgd_padre);
>>>>>>> 2eb9a37c5628868137b514de958adcca3ea74aea

END$$

DELIMITER ;

-- -----------------------------------------------------
<<<<<<< HEAD
-- procedure new_animal
=======
-- procedure new_animal_oscar
>>>>>>> 2eb9a37c5628868137b514de958adcca3ea74aea
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
<<<<<<< HEAD
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_animal`(IN rp VARCHAR(3),
IN n_arete VARCHAR(4),IN id_area VARCHAR(30),IN nom VARCHAR(15),
IN sexo VARCHAR(10),IN fecha_llega DATE,IN haci_ori VARCHAR(15),
IN propi VARCHAR(30),IN rp_madre VARCHAR(3),IN rp_padre VARCHAR(3),
IN estado VARCHAR(10),IN raza VARCHAR(15),IN rgd VARCHAR(15),IN rgd_padre VARCHAR(15))
=======
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_animal_oscar`(IN rp VARCHAR(3),
IN n_arete VARCHAR(4),IN id_area VARCHAR(30),IN nom VARCHAR(15),
IN sexo VARCHAR(10),IN fecha_llega DATE,IN haci_ori VARCHAR(15),
IN propi VARCHAR(30),IN rp_madre VARCHAR(3),IN rp_padre VARCHAR(3),
IN estado VARCHAR(10),IN raza VARCHAR(15), IN rgd VARCHAR(10), 
IN rgd_padre VARCHAR(10))
>>>>>>> 2eb9a37c5628868137b514de958adcca3ea74aea
BEGIN
INSERT INTO animal values(rp,n_arete,
(SELECT codigo FROM area  WHERE nombre = id_area),nom,sexo,fecha_llega,haci_ori,
(SELECT cedula FROM persona  WHERE nombre = propi),
(select a.rp from animal a where a.rp=rp_madre),
(select a.rp from animal a where a.rp=rp_padre),estado,raza, rgd, rgd_padre);

<<<<<<< HEAD
INSERT INTO animal values(rp,n_arete,
(SELECT codigo FROM area  WHERE nombre = id_area),nom,sexo,fecha_llega,haci_ori,
(SELECT cedula FROM persona  WHERE nombre = propi),
(select a.rp from animal a where a.rp=rp_madre),
(select a.rp from animal a where a.rp=rp_padre),estado,raza,rgd,rgd_padre);

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_animal_oscar
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_animal_oscar`(IN rp VARCHAR(3),
IN n_arete VARCHAR(4),IN id_area VARCHAR(30),IN nom VARCHAR(15),
IN sexo VARCHAR(10),IN fecha_llega DATE,IN haci_ori VARCHAR(15),
IN propi VARCHAR(30),IN rp_madre VARCHAR(3),IN rp_padre VARCHAR(3),
IN estado VARCHAR(10),IN raza VARCHAR(15), IN rgd VARCHAR(10), 
IN rgd_padre VARCHAR(10))
BEGIN
INSERT INTO animal values(rp,n_arete,
(SELECT codigo FROM area  WHERE nombre = id_area),nom,sexo,fecha_llega,haci_ori,
(SELECT cedula FROM persona  WHERE nombre = propi),
(select a.rp from animal a where a.rp=rp_madre),
(select a.rp from animal a where a.rp=rp_padre),estado,raza, rgd, rgd_padre);

=======
>>>>>>> 2eb9a37c5628868137b514de958adcca3ea74aea
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_area
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_area`(IN cod INT(11),IN nom VARCHAR(30), IN dir VARCHAR(40),IN dim float,IN capMax VARCHAR(11))
BEGIN

INSERT INTO area

VALUES(cod,nom,dir,dim,capMax);

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_baja
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_baja`(IN idbaja INT(11), IN fecha DATE, IN tipo VARCHAR(10), IN pesolb FLOAT, IN preciolb FLOAT, IN rp VARCHAR(3))
BEGIN

INSERT INTO bajas values(idbaja, fecha, tipo, pesolb, preciolb, rp, (pesolb*preciolb));

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_edad
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_edad`(IN rp_edad VARCHAR(3),IN fecha_naci_edad date)
BEGIN
declare edadAnios,edadMeses,edadDias int;
set edadAnios=(YEAR( CURDATE( ) ) - YEAR(fecha_naci_edad)) - 
	IF( MONTH( CURDATE( ) ) < MONTH(fecha_naci_edad), 1, 
		IF (MONTH(CURDATE( )) = MONTH(fecha_naci_edad), 
			IF (DAY( CURDATE( ) ) < DAY(fecha_naci_edad),1,0 
		),0
	)
);
set edadMeses=MONTH(CURDATE()) - MONTH(fecha_naci_edad) + 12 * IF( MONTH(CURDATE())<MONTH(fecha_naci_edad), 1,IF(MONTH(CURDATE())=MONTH(fecha_naci_edad),IF (DAY(CURDATE())<DAY(fecha_naci_edad),1,0),0)
) - IF(MONTH(CURDATE())<>MONTH(fecha_naci_edad),(DAY(CURDATE())<DAY(fecha_naci_edad)), IF (DAY(CURDATE())<DAY(fecha_naci_edad),1,0 ) );
set edadDias=(
DAY( CURDATE( ) ) - DAY(fecha_naci_edad) +30 * ( DAY(CURDATE( )) < DAY(fecha_naci_edad) ));
insert into edades values(rp_edad ,fecha_naci_edad,edadAnios,edadMeses,edadDias,clasificacionPorEdad(edadAnios,edadMeses));
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_hacienda
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_hacienda`(IN ruc VARCHAR(13),IN nom VARCHAR(20),IN ci_prop VARCHAR(10),IN email VARCHAR(30), IN id INT(11))
BEGIN
INSERT INTO hacienda
VALUES(ruc,nom,ci_prop,email, id);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_medicamento
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_medicamento`(IN nombre VARCHAR(20), IN tipo VARCHAR(10), IN periodicidad VARCHAR(10),IN posologia VARCHAR(20))
BEGIN

INSERT INTO medicamento(nombre,tipo,periodicidad,posologia)

VALUES(nombre,tipo,periodicidad,posologia);

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_parto
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_parto`(IN rp_padre VARCHAR(10),IN fecha_parto DATE,IN sexo VARCHAR(10),IN estado VARCHAR(10),IN id_parto VARCHAR(5),IN rp_vaca VARCHAR(3))
BEGIN

insert into partos values(rp_padre,fecha_parto,sexo,estado,id_parto,(select rp from animal where rp=rp_vaca));

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_persona
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_persona`(IN ced VARCHAR(10),IN nom VARCHAR(30),IN dir VARCHAR(20),IN conven VARCHAR(9),IN movil VARCHAR(10), IN id INT(11))
BEGIN

INSERT INTO persona

VALUES(ced,nom,dir,conven,movil, id);

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_pesaje
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_pesaje`(IN id_pesaje VARCHAR(10),IN rp_vaca VARCHAR(3),IN peso float,IN fecha_pesaje DATE)
BEGIN

insert into pesaje values(id_pesaje,(select rp from animal where rp=rp_vaca),peso,fecha_pesaje);

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_planRepro
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_planRepro`(IN id_repro VARCHAR(5),IN rp_padre VARCHAR(10),IN rp_vaca VARCHAR(3),IN apli_dispo BIT(1),IN condi_corpo float,in palpacion BIT(1),IN comentario VARCHAR(100),IN tipo_repro VARCHAR(20),in fecha_plan DATE)
BEGIN

insert into plan_reproductivo values(id_repro,rp_padre,(select rp from animal where rp=rp_vaca),apli_dispo,condi_corpo,palpacion,comentario,tipo_repro,fecha_plan);

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure new_planSani
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
<<<<<<< HEAD
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_planSani`(IN id_plan int(11),
IN nombre VARCHAR(20),IN tipo VARCHAR(10),IN fecha_apli date,IN rp_vaca VARCHAR(3),
IN nombre_med VARCHAR(20),IN dosis float)
=======
CREATE DEFINER=`root`@`localhost` PROCEDURE `new_planSani`(IN id_plan int(11),IN nombre VARCHAR(20),IN tipo VARCHAR(10),IN fecha_apli datetime,IN rp_vaca VARCHAR(3),IN nombre_med VARCHAR(20),IN dosis float)
>>>>>>> 2eb9a37c5628868137b514de958adcca3ea74aea
BEGIN

insert into plan_sanitario(id_plan,nombre,tipo,fecha_aplicacion,rp_vaca,nombre_med,dosis)

 values(id_plan,nombre,tipo,fecha_apli,(select rp from animal where rp=rp_vaca),nombre_med,dosis);

END$$

DELIMITER ;

-- -----------------------------------------------------
-- function noTiene_fecha
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `noTiene_fecha`(fecha DATE ) RETURNS varchar(20) CHARSET utf8
BEGIN



	RETURN IFNULL(fecha,"No tiene");



END$$
<<<<<<< HEAD

DELIMITER ;

-- -----------------------------------------------------
-- function num_hijos
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `num_hijos`(sexo varchar(10),num_hijos int) RETURNS varchar(15) CHARSET utf8
BEGIN

if(sexo="hembra") then

	RETURN num_hijos;

else

	RETURN "No Aplica";

=======

DELIMITER ;

-- -----------------------------------------------------
-- function num_hijos
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `num_hijos`(sexo varchar(10),num_hijos int) RETURNS varchar(15) CHARSET utf8
BEGIN

if(sexo="hembra") then

	RETURN num_hijos;

else

	RETURN "No Aplica";

>>>>>>> 2eb9a37c5628868137b514de958adcca3ea74aea
end if;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure reporte_bajas
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `reporte_bajas`(IN fecha_desde DATE,IN fecha_hasta DATE)
BEGIN

select id_baja,rp_vaca,fecha_baja,tipo_baja,peso_libras,precio_por_libras

from bajas

where (fecha_baja between fecha_desde  and fecha_hasta);

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure reporte_pesaje
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `reporte_pesaje`(IN fecha_desde DATE,IN fecha_hasta DATE)
BEGIN

select p.idpesaje,p.rp_vaca,p.fecha_pesaje,e.edades_años,e.edades_meses,e.edades_dias,p.peso 

from pesaje p,edades e

where p.rp_vaca=e.rp and (p.fecha_pesaje between fecha_desde  and fecha_hasta);
<<<<<<< HEAD

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure reporte_planRepro
-- -----------------------------------------------------

=======

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure reporte_planRepro
-- -----------------------------------------------------

>>>>>>> 2eb9a37c5628868137b514de958adcca3ea74aea
DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `reporte_planRepro`(IN fecha_desde DATE,IN fecha_hasta DATE)
BEGIN

select id_reproduccion,rp_padre,fecha_plan,si_no(aplica_dispositivo),condicion_corporal,comentario,tipo_reproduccion,si_no(palpacion)

from plan_reproductivo 

where (fecha_plan between fecha_desde  and fecha_hasta);

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure reporte_plan_sanitario
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `reporte_plan_sanitario`(IN fecha_desde DATE,IN fecha_hasta DATE)
BEGIN

select plan_sanitario.id_plan, plan_sanitario.nombre, plan_sanitario.fecha_aplicacion, plan_sanitario.tipo,

plan_sanitario.nombre_med, medicamento.posologia, medicamento.periodicidad, plan_sanitario.dosis, pesaje.peso

from medicamento, plan_sanitario, animal, pesaje

where medicamento.nombre = plan_sanitario.nombre_med and animal.RP = plan_sanitario.rp_vaca and animal.RP = pesaje.rp_vaca and (plan_sanitario.fecha_aplicacion between fecha_desde  and fecha_hasta);

END$$

DELIMITER ;

-- -----------------------------------------------------
-- function si_no
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `si_no`(v bool,sexo varchar(10)) RETURNS varchar(15) CHARSET utf8
BEGIN

	

	if(v=1 and sexo="hembra") then

		RETURN "Está preñada";

	

	elseif(v=0 and sexo="hembra") then

		RETURN "No está preñada";

	else

		if(sexo="hembra") then

			RETURN "No tiene Plan";

        else

			RETURN "No Aplica";

		end if;

    end if;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- function si_no_dgvplanrepro
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `si_no_dgvplanrepro`(v bool) RETURNS varchar(3) CHARSET utf8
BEGIN
if(v=1 ) then
	RETURN "Sí";
else
	RETURN "No";
end if;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure update_area
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_area`(IN cod INT(11),IN nom VARCHAR(30), IN dir VARCHAR(40),IN dim float,IN capMax VARCHAR(11))
BEGIN

update area

set area.nombre = nom, area.direccion = dir, area.dimension = dim, area.capacidadMaxima = capMax

where area.codigo = cod;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure update_baja
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_baja`(IN idbaja INT(11), IN fecha DATE, IN tipo VARCHAR(10), IN pesolb FLOAT, IN preciolb FLOAT, IN rp VARCHAR(3))
BEGIN

update bajas

set bajas.fecha_baja = fecha,  bajas.tipo_baja = tipo, bajas.peso_libra = pesolb, bajas.precio_por_libras = preciolb, bajas.rp_vaca = rp, 

bajas.total = (pesolb*preciolb)

where bajas.id_baja = idbaja;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure update_control_gestacion
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_control_gestacion`()
BEGIN

SET SQL_SAFE_UPDATES = 0;


update control_gestacion 

set meses_gestacion=MONTH(CURDATE()) - MONTH(fecha_concepcion) + 12 * IF( MONTH(CURDATE())<MONTH(fecha_concepcion), 1,IF(MONTH(CURDATE())=MONTH(fecha_concepcion),IF (DAY(CURDATE())<DAY(fecha_concepcion),1,0),0)

) - IF(MONTH(CURDATE())<>MONTH(fecha_concepcion),(DAY(CURDATE())<DAY(fecha_concepcion)), IF (DAY(CURDATE())<DAY(fecha_concepcion),1,0 ) ),




dias_gestacion=(DAY( CURDATE( ) ) - DAY(fecha_concepcion) +30 * ( DAY(CURDATE( )) < DAY(fecha_concepcion) ))

where id_reproduccion not  in ((select a.id_reproduccion from (select  p.id_reproduccion,p.rp_vaca from plan_reproductivo p,control_gestacion c where c.id_reproduccion = p.id_reproduccion and p.palpacion=true and p.rp_vaca  in ( select rp_vaca from partos ) order by p.fecha_plan desc)a

 group by a.rp_vaca ));




END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure update_edades
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_edades`()
BEGIN

SET SQL_SAFE_UPDATES = 0;

update edades

set edades_años=(YEAR( CURDATE( ) ) - YEAR(fecha_nacimiento)) - 

	IF( MONTH( CURDATE( ) ) < MONTH(fecha_nacimiento), 1, 

		IF (MONTH(CURDATE( )) = MONTH(fecha_nacimiento), 

			IF (DAY( CURDATE( ) ) < DAY(fecha_nacimiento),1,0 

		),0

	)

),

edades_meses=MONTH(CURDATE()) - MONTH(fecha_nacimiento) + 12 * IF( MONTH(CURDATE())<MONTH(fecha_nacimiento), 1,IF(MONTH(CURDATE())=MONTH(fecha_nacimiento),IF (DAY(CURDATE())<DAY(fecha_nacimiento),1,0),0)

) - IF(MONTH(CURDATE())<>MONTH(fecha_nacimiento),(DAY(CURDATE())<DAY(fecha_nacimiento)), IF (DAY(CURDATE())<DAY(fecha_nacimiento),1,0 ) ),

edades_dias=DAY( CURDATE( ) ) - DAY(fecha_nacimiento) +30 * ( DAY(CURDATE( )) < DAY(fecha_nacimiento) )



,clasificacion=clasificacionPorEdad(edades_años,edades_meses);

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure update_persona
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_persona`(IN ced VARCHAR(10),IN nom VARCHAR(30),IN dir VARCHAR(20),IN conven VARCHAR(9),IN movil VARCHAR(10), IN id INT(11))
BEGIN

update persona

set persona.cedula = ced, persona.nombre = nom, persona.direccion = dir, persona.telefono_casa = conven, persona.telefono_movil = movil

where persona.id_persona = id;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure update_planRepro
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_planRepro`(IN id_repro int(5),IN rp_padre VARCHAR(10),IN rp_vaca VARCHAR(3),IN apli_dispo BIT(1),IN condi_corpo float,in palpacion BIT(1),IN comentario VARCHAR(100),IN tipo_repro VARCHAR(20),in fecha_plan timestamp)
BEGIN
update plan_reproductivo p
set p.rp_padre=rp_padre,p.rp_vaca=rp_vaca,p.aplica_dispositivo=apli_dispo,p.condicion_corporal=condi_corpo,p.palpacion=palpacion,p.comentario=comentario,p.tipo_reproduccion=tipo_repro,p.fecha_plan=fecha_plan
where p.id_reproduccion=id_repro;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure update_planSani
-- -----------------------------------------------------

DELIMITER $$
USE `discosdurostest`$$
<<<<<<< HEAD
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_planSani`(IN id_plan int(11),IN nombre VARCHAR(20),IN tipo VARCHAR(10),IN fecha_apli date,IN rp_vaca VARCHAR(3),IN nombre_med VARCHAR(20),IN dosis float)
=======
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_planSani`(IN id_plan int(11),IN nombre VARCHAR(20),IN tipo VARCHAR(10),IN fecha_apli datetime,IN rp_vaca VARCHAR(3),IN nombre_med VARCHAR(20),IN dosis float)
>>>>>>> 2eb9a37c5628868137b514de958adcca3ea74aea
BEGIN

update plan_sanitario

set plan_sanitario.nombre = nombre, plan_sanitario.tipo = tipo, plan_sanitario.fecha_aplicacion = fecha_apli, plan_sanitario.rp_vaca = rp_vaca, plan_sanitario.nombre_med = nombre_med, plan_sanitario.dosis = dosis

where plan_sanitario.id_plan = id_plan;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- View `discosdurostest`.`bajas_view`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `discosdurostest`.`bajas_view`;
USE `discosdurostest`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `discosdurostest`.`bajas_view` AS select `discosdurostest`.`bajas`.`id_baja` AS `id_baja`,`discosdurostest`.`bajas`.`rp_vaca` AS `rp_vaca`,`discosdurostest`.`bajas`.`fecha_baja` AS `fecha_baja`,`discosdurostest`.`bajas`.`tipo_baja` AS `tipo_baja`,`discosdurostest`.`bajas`.`peso_libras` AS `peso_libras`,`discosdurostest`.`bajas`.`precio_por_libras` AS `precio_por_libras` from `discosdurostest`.`bajas`;

-- -----------------------------------------------------
-- View `discosdurostest`.`numeros_parto_por_animal`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `discosdurostest`.`numeros_parto_por_animal`;
USE `discosdurostest`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `discosdurostest`.`numeros_parto_por_animal` AS (select `a`.`RP` AS `RP`,`a`.`n_arete` AS `n_arete`,`a`.`id_area` AS `id_area`,`a`.`nombre` AS `nombre`,`a`.`sexo` AS `sexo`,`a`.`fecha_llegada` AS `fecha_llegada`,`a`.`hacienda_origen` AS `hacienda_origen`,`a`.`propietario` AS `propietario`,`a`.`rp_madre` AS `rp_madre`,`a`.`rp_padre` AS `rp_padre`,`a`.`estado` AS `estado`,`a`.`raza` AS `raza`,count(`p`.`rp_vaca`) AS `num_partos` from (`discosdurostest`.`animal` `a` left join `discosdurostest`.`partos` `p` on((`a`.`RP` = `p`.`rp_vaca`))) group by `a`.`RP`);
USE `discosdurostest`;

DELIMITER $$
USE `discosdurostest`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `discosdurostest`.`ingresar_control_gestacion`
AFTER INSERT ON `discosdurostest`.`plan_reproductivo`
FOR EACH ROW
BEGIN

declare MesesGes,DiasGes int;

	declare new_fecha date;

    set new_fecha=new.fecha_plan;



if (new.palpacion = true ) then 

	set MesesGes=MONTH(CURDATE()) - MONTH(new_fecha) + 12 * IF( MONTH(CURDATE())<MONTH(new_fecha), 1,IF(MONTH(CURDATE())=MONTH(new_fecha),IF (DAY(CURDATE())<DAY(new_fecha),1,0),0)

) - IF(MONTH(CURDATE())<>MONTH(new_fecha),(DAY(CURDATE())<DAY(new_fecha)), IF (DAY(CURDATE())<DAY(new_fecha),1,0 ) );

	set DiasGes=(

DAY( CURDATE( ) ) - DAY(new_fecha) +30 * ( DAY(CURDATE( )) < DAY(new_fecha) ));

	insert into control_gestacion

	values(new.id_reproduccion,new.fecha_plan,MesesGes,DiasGes,DATE_ADD(new.fecha_plan, INTERVAL 10 MONTH ) );

end if;



END$$


DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
