

INSERT INTO `discosdurostest`.`persona` (`cedula`, `nombre`, `direccion`, `telefono_casa`, `telefono_movil`, `id_persona`) VALUES ('0931245229', 'Oscar', 'albo', '2275857', '0939181841', '9');
INSERT INTO `discosdurostest`.`persona` (`cedula`, `nombre`, `direccion`, `telefono_casa`, `telefono_movil`, `id_persona`) VALUES ('0904042363', 'raul', 'albo 4', '2223232', '0983818282', '10');
INSERT INTO `discosdurostest`.`persona` (`cedula`, `nombre`, `direccion`, `telefono_casa`, `telefono_movil`, `id_persona`) VALUES ('0904042312', 'Carla', 'sauce', '2213123', '0923211113', '8');
INSERT INTO `discosdurostest`.`persona` (`cedula`, `nombre`, `direccion`, `telefono_casa`, `telefono_movil`, `id_persona`) VALUES ('0932312347', 'Mar castro', 'saucin', '2993923', '0947372818', '7');
INSERT INTO `discosdurostest`.`persona` (`cedula`, `nombre`, `direccion`, `telefono_casa`, `telefono_movil`, `id_persona`) VALUES ('0934347571', 'Juan Lopez', 'muran de rio', '2394848', '0949281847', '6');


INSERT INTO `discosdurostest`.`area` (`codigo`, `nombre`, `direccion`, `dimension`, `capacidadMaxima`) VALUES ('1', 'NAPI', 'sambo', '100', '50');
INSERT INTO `discosdurostest`.`area` (`codigo`, `nombre`, `direccion`, `dimension`, `capacidadMaxima`) VALUES ('2', 'VICENTE', 'portoviejo', '300', '150');
INSERT INTO `discosdurostest`.`area` (`codigo`, `nombre`, `direccion`, `dimension`, `capacidadMaxima`) VALUES ('3', 'BOLITA', 'duran', '70', '30');
INSERT INTO `discosdurostest`.`area` (`codigo`, `nombre`, `direccion`, `dimension`, `capacidadMaxima`) VALUES ('4', 'LAGUNA', 'duran',  '123', '123');
INSERT INTO `discosdurostest`.`area` (`codigo`, `nombre`, `direccion`, `dimension`, `capacidadMaxima`) VALUES ('5', 'SAN FELIPE', 'daule 4 ra', '45', '32');


INSERT INTO `discosdurostest`.`animal` (`RP`, `n_arete`, `id_area`, `nombre`, `sexo`, `fecha_llegada`, `hacienda_origen`, `propietario`, `estado`, `raza`) VALUES ('003', '3', '5', 'mah', 'MACHO', '2014-01-24', '', '0931245229', 'ACTIVO', 'HOLSTEIN');
INSERT INTO `discosdurostest`.`animal` (`RP`, `n_arete`, `id_area`, `nombre`, `sexo`, `fecha_llegada`, `propietario`, `estado`, `raza`) VALUES ('001', '1', '1', 'meme', 'HEMBRA', '2015-05-30', '0932312347', 'ACTIVO', 'BEEFMASTER');
INSERT INTO `discosdurostest`.`animal` (`RP`, `n_arete`, `id_area`, `nombre`, `sexo`, `fecha_llegada`, `propietario`, `estado`, `raza`) VALUES ('002', '2', '2', 'pepe', 'HEMBRA', '2013-04-22', '0934347571', 'ACTIVO', 'HEREFORD');
INSERT INTO `discosdurostest`.`animal` (`RP`, `n_arete`, `id_area`, `nombre`, `sexo`, `fecha_llegada`, `propietario`, `estado`, `raza`) VALUES ('004', '4', '5', 'meh', 'HEMBRA', '2014-03-21', '0931245229', 'ACTIVO', 'HOLSTEIN');
INSERT INTO `discosdurostest`.`animal` (`RP`, `n_arete`, `id_area`, `nombre`, `sexo`, `fecha_llegada`, `propietario`, `rp_madre`, `rp_padre`, `estado`, `raza`) VALUES ('005', '5', '5', 'mih', 'HEMBRA', '2015-09-11', '0931245229', '004', '003', 'ACTIVO', 'HOLSTEIN');



INSERT INTO `discosdurostest`.`edades` (`RP`, `fecha_nacimiento`, `edades_años`, `edades_meses`, `edades_dias`) VALUES ('001', '2013-05-30', '3', '4', '4','Vacona');
INSERT INTO `discosdurostest`.`edades` (`RP`, `fecha_nacimiento`, `edades_años`, `edades_meses`, `edades_dias`, `clasificacion`) VALUES ('002', '2010-04-22', '6', '3', '4', 'Vaca');
INSERT INTO `discosdurostest`.`edades` (`RP`, `fecha_nacimiento`, `edades_años`, `edades_meses`, `edades_dias`, `clasificacion`) VALUES ('003', '2010-12-20', '6', '1', '6', 'Vaca');
INSERT INTO `discosdurostest`.`edades` (`RP`, `fecha_nacimiento`, `edades_años`, `edades_meses`, `edades_dias`, `clasificacion`) VALUES ('004', '2011-03-21', '5', '2', '5', 'Vaca');
INSERT INTO `discosdurostest`.`edades` (`RP`, `fecha_nacimiento`, `edades_años`, `edades_meses`, `edades_dias`, `clasificacion`) VALUES ('005', '2015-09-11', '0', '4', '15', 'Ternera');


INSERT INTO `discosdurostest`.`medicamento` (`nombre`, `tipo`, `periodicidad`, `posologia`, `id_medicamento`) VALUES ('3453dfs', 'INTERNO', '53', '45', '10');
