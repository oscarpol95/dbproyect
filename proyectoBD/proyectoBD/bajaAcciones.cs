﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
namespace WindowsFormsApplication1
{
    class bajaAcciones
    {
        public static int Agregar(baja baja)
        {

            int retorno = 0;
            MySqlCommand cmd = new MySqlCommand("new_baja", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("idbaja", null));
            cmd.Parameters.Add(new MySqlParameter("fecha", baja.fecha));
            cmd.Parameters.Add(new MySqlParameter("tipo", baja.tipo));
            cmd.Parameters.Add(new MySqlParameter("pesolb", baja.pesolb));
            cmd.Parameters.Add(new MySqlParameter("preciolb", baja.preciolb));
            cmd.Parameters.Add(new MySqlParameter("rp", baja.rpvaca));
            cmd.Parameters.Add(new MySqlParameter("detalle", baja.detalle));
            retorno = cmd.ExecuteNonQuery();

            return retorno;
        }

        public static int Eliminar(string id, string rp)
        {
            int retorno = 0;

            MySqlCommand cmd = new MySqlCommand("borrar_baja", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("id_baja", id));
            cmd.Parameters.Add(new MySqlParameter("rp", rp));
            retorno = cmd.ExecuteNonQuery();
            coneccionBase.ObtenerConexion().Close();

            return retorno;
        }

        public static int Actualizar(baja baja)
        {

            int retorno = 0;
            MySqlCommand cmd = new MySqlCommand("update_baja", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("idbaja", baja.idbaja));
            cmd.Parameters.Add(new MySqlParameter("fecha", baja.fecha));
            cmd.Parameters.Add(new MySqlParameter("tipo", baja.tipo));
            cmd.Parameters.Add(new MySqlParameter("pesolb", baja.pesolb));
            cmd.Parameters.Add(new MySqlParameter("preciolb", baja.preciolb));
            cmd.Parameters.Add(new MySqlParameter("rp", baja.rpvaca));
            cmd.Parameters.Add(new MySqlParameter("detalle", baja.detalle));
            retorno = cmd.ExecuteNonQuery();

            return retorno;
        }

    }
}

