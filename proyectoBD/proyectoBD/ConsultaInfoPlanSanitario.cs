﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    public partial class ConsultaInfoPlanSanitario : Form
    {
        public ConsultaInfoPlanSanitario()
        {
            InitializeComponent();
        }
        
        public IngresoPlanSanitario plan = new IngresoPlanSanitario(false);
        

        private void lblNuevo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //WindowsFormsApplication1.IngresoPlanSanitario plan = new WindowsFormsApplication1.IngresoPlanSanitario();
            plan.txtrp.Text = txtRP.Text;
            plan.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            plan.Show();
            this.Hide();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void otherForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            dataGridView1.Rows.Clear();
            ConsultaInfoPlanSanitario_Load(sender, e);
            this.Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           if (e.ColumnIndex == 1 && e.RowIndex >= 0)
            {
                 IngresoPlanSanitario plan = new IngresoPlanSanitario(true);
                 //WindowsFormsApplication1.IngresoPlanSanitario plan = new WindowsFormsApplication1.IngresoPlanSanitario(true);

                string celda = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex-1].Value.ToString();

                MySqlCommand cmd = new MySqlCommand("buscar_plan_sanitario_tabla", coneccionBase.ObtenerConexion());
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("cod", celda));

                
                plan.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                plan.txtrp.Text = this.txtRP.Text;
                plan.Show();
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
               // planSani.fecha_apli = dtfecha.Value.Year + "/" + dtfecha.Value.Month + "/" + dtfecha.Value.Day;
                
                plan.txtID.Text = reader.GetString(0);
                plan.txtnombre.Text = reader.GetString(1);
                plan.cmbtipo.Text = reader.GetString(2);
                plan.dtfecha.Text = reader.GetString(3);
                plan.cmbmedicamento.Text = reader.GetString(5);
                plan.txtdosis.Text = reader.GetString(6);

                this.Hide();

                
                //WindowsFormsApplication1.IngresoPlanSanitario plan = new WindowsFormsApplication1.IngresoPlanSanitario();
               /* plan.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                plan.Show();
                this.Hide();*/
            }
        }


        private void ConsultaInfoPlanSanitario_Load(object sender, EventArgs e)
        {
            MySqlCommand cmd = new MySqlCommand("buscar_plan_sanitario", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("rp", txtRP.Text));

            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
                dataGridView1.Rows.Add(true, reader.GetString(0),reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5), reader.GetString(6), reader.GetString(7));
        }
        private void lblEliminar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            List<DataGridViewRow> toDelete = new List<DataGridViewRow>();
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (Convert.ToBoolean(row.Cells[0].Value) == true)
                {
                    toDelete.Add(row);
                }
            }
            if (!toDelete.Any())
                MessageBox.Show("Debe seleccionar una fila");
            else
            {
                foreach (DataGridViewRow row in toDelete)
                {
                    string nom = Convert.ToString(row.Cells[1].Value);
                    planSaniAcciones.Eliminar(nom);
                    dataGridView1.Rows.Remove(row);
                }
            }

        }

        private void btnMostrar_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            MySqlCommand cmd = new MySqlCommand("buscar_plan_sani_por_fecha", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("rp", txtRP.Text));
            string fechaDesde = dateDesde.Value.Year + "/" + dateDesde.Value.Month + "/" + dateDesde.Value.Day;
            string fechaHasta = dateHasta.Value.Year + "/" + dateHasta.Value.Month + "/" + dateHasta.Value.Day;
            cmd.Parameters.Add(new MySqlParameter("fecha_desde", fechaDesde));
            cmd.Parameters.Add(new MySqlParameter("fecha_hasta", fechaHasta));
            MySqlDataReader reader = cmd.ExecuteReader();


            while (reader.Read())
                dataGridView1.Rows.Add(true, reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5), reader.GetString(6), reader.GetString(7));
        }
    }
}
