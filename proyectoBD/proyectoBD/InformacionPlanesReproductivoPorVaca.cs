﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace WindowsFormsApplication1
{
    public partial class InformacionPlanesReproductivoPorVaca : Form
    {
        public InformacionPlanesReproductivoPorVaca()
        {
            InitializeComponent();
        }

        private void historialDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3 && e.RowIndex >= 0)
            {
                string nombre = dgvplanrepro.Rows[e.RowIndex].Cells[e.ColumnIndex - 2].Value.ToString();
                WindowsFormsApplication1.IngresoPlanReproductivo plan = new WindowsFormsApplication1.IngresoPlanReproductivo(true);
                plan.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                plan.txtVaca.Text = this.txtrp.Text;
                MySqlCommand cmd = new MySqlCommand("buscar_pla_repro_individual", coneccionBase.ObtenerConexion());
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("cod", nombre));

                plan.Show();
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                plan.txtID.Text = reader.GetString(0);
                plan.txtToro.Text = reader.GetString(1);
                plan.txtCondicion.Text = reader.GetString(4);
                plan.cmbTipo.Text = reader.GetString(7);
                plan.datePlan.Text = reader.GetString(8);
                plan.txtComentario.Text = reader.GetString(6);
                plan.chbDispo.Checked = reader.GetBoolean(3);
                plan.chbPrena.Checked = reader.GetBoolean(5);
                this.Hide();
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lnkNuevo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            WindowsFormsApplication1.IngresoPlanReproductivo plan = new WindowsFormsApplication1.IngresoPlanReproductivo(false);
            plan.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            plan.txtVaca.Text = this.txtrp.Text;
            plan.Show();
            this.Hide();
        }

        void otherForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            dgvplanrepro.Rows.Clear();
            InformacionPlanesReproductivoPorVaca_Load(sender, e);
            this.Show();
        }

        private void InformacionPlanesReproductivoPorVaca_Load(object sender, EventArgs e)
        {
            MySqlCommand cmd = new MySqlCommand("buscar_plan_repro", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("rp", txtrp.Text));

            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
                dgvplanrepro.Rows.Add(false, reader.GetString(0),reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5), reader.GetString(6), reader.GetString(7));





            List<String> lista = new List<String>();
            MySqlCommand cmd1 = new MySqlCommand("buscar_cont_ges", coneccionBase.ObtenerConexion());
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.Parameters.Add(new MySqlParameter("rp", txtrp.Text));

            MySqlDataReader reader1 = cmd1.ExecuteReader();
            while (reader1.Read())
            {

                lista.Add(reader1.GetString(0));

            }
            if (lista.Count > 0 && reader1.GetString(1) == "No")
            {
                txtestaprenada.Text = reader1.GetString(1);
                txtMesesgestacion.Text = "";
                txtdiasgestacion.Text = "";
            }
            else if (lista.Count > 0)
            {

                txtestaprenada.Text = reader1.GetString(1);

                txtMesesgestacion.Text = reader1.GetString(2);

                txtdiasgestacion.Text = reader1.GetString(3);

            }

            else
            {
                txtestaprenada.Text = "";
                txtMesesgestacion.Text = "";
                txtdiasgestacion.Text = "";
            }
        }

        private void lnkEliminar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            List<DataGridViewRow> toDelete = new List<DataGridViewRow>();
            foreach (DataGridViewRow row in dgvplanrepro.Rows)
            {
                if (Convert.ToBoolean(row.Cells[0].Value) == true)
                {
                    toDelete.Add(row);
                }
            }
            if (!toDelete.Any())
                MessageBox.Show("Debe seleccionar una fila");
            else
            {
                foreach (DataGridViewRow row in toDelete)
                {
                    string nom = Convert.ToString(row.Cells[1].Value);
                    planReproAcciones.Eliminar(nom);
                    dgvplanrepro.Rows.Remove(row);
                }
            }
        }

        private void btnMostrar_Click(object sender, EventArgs e)
        {
            dgvplanrepro.Rows.Clear();
            MySqlCommand cmd = new MySqlCommand("buscar_plan_repro_fecha", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("rp", txtrp.Text));
            string fechaDesde = dateDesde.Value.Year + "/" + dateDesde.Value.Month + "/" + dateDesde.Value.Day;
            string fechaHasta = dateHasta.Value.Year + "/" + dateHasta.Value.Month + "/" + dateHasta.Value.Day;
            cmd.Parameters.Add(new MySqlParameter("fecha_desde", fechaDesde));
            cmd.Parameters.Add(new MySqlParameter("fecha_hasta", fechaHasta));
            MySqlDataReader reader = cmd.ExecuteReader();


            while (reader.Read())
                dgvplanrepro.Rows.Add(false, reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5), reader.GetString(6), reader.GetString(7));
        }
    }
}
