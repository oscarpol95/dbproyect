﻿namespace WindowsFormsApplication1
{
    partial class IngresoBajas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IngresoBajas));
            this.btndescartar = new System.Windows.Forms.Button();
            this.btnguardar = new System.Windows.Forms.Button();
            this.cmbtipo = new System.Windows.Forms.ComboBox();
            this.txtprecio = new System.Windows.Forms.TextBox();
            this.txtpeso = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.fechaBaja = new System.Windows.Forms.Label();
            this.peso = new System.Windows.Forms.Label();
            this.rpBaja = new System.Windows.Forms.Label();
            this.dtfecha = new System.Windows.Forms.DateTimePicker();
            this.btn_editar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtID = new System.Windows.Forms.TextBox();
            this.cmbRP = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtdetalle = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btndescartar
            // 
            this.btndescartar.Location = new System.Drawing.Point(312, 176);
            this.btndescartar.Name = "btndescartar";
            this.btndescartar.Size = new System.Drawing.Size(92, 40);
            this.btndescartar.TabIndex = 63;
            this.btndescartar.Text = "Descartar";
            this.btndescartar.UseVisualStyleBackColor = true;
            this.btndescartar.Click += new System.EventHandler(this.btndescartar_Click);
            // 
            // btnguardar
            // 
            this.btnguardar.Location = new System.Drawing.Point(173, 176);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.Size = new System.Drawing.Size(92, 40);
            this.btnguardar.TabIndex = 62;
            this.btnguardar.Text = "Guardar";
            this.btnguardar.UseVisualStyleBackColor = true;
            this.btnguardar.Click += new System.EventHandler(this.btnguardar_Click);
            // 
            // cmbtipo
            // 
            this.cmbtipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbtipo.FormattingEnabled = true;
            this.cmbtipo.Items.AddRange(new object[] {
            "Muerte",
            "Venta"});
            this.cmbtipo.Location = new System.Drawing.Point(397, 11);
            this.cmbtipo.Name = "cmbtipo";
            this.cmbtipo.Size = new System.Drawing.Size(121, 21);
            this.cmbtipo.TabIndex = 61;
            this.cmbtipo.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // txtprecio
            // 
            this.txtprecio.Location = new System.Drawing.Point(397, 66);
            this.txtprecio.MaxLength = 5;
            this.txtprecio.Name = "txtprecio";
            this.txtprecio.Size = new System.Drawing.Size(100, 20);
            this.txtprecio.TabIndex = 60;
            this.txtprecio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtprecio_KeyPress);
            // 
            // txtpeso
            // 
            this.txtpeso.Location = new System.Drawing.Point(115, 62);
            this.txtpeso.MaxLength = 5;
            this.txtpeso.Name = "txtpeso";
            this.txtpeso.Size = new System.Drawing.Size(100, 20);
            this.txtpeso.TabIndex = 57;
            this.txtpeso.TextChanged += new System.EventHandler(this.txtpeso_TextChanged);
            this.txtpeso.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtpeso_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(320, 69);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 55;
            this.label6.Text = "Precio por lbs:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(320, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 54;
            this.label5.Text = "Tipo:";
            // 
            // fechaBaja
            // 
            this.fechaBaja.AutoSize = true;
            this.fechaBaja.Location = new System.Drawing.Point(17, 116);
            this.fechaBaja.Name = "fechaBaja";
            this.fechaBaja.Size = new System.Drawing.Size(76, 13);
            this.fechaBaja.TabIndex = 52;
            this.fechaBaja.Text = "Fecha de Baja";
            // 
            // peso
            // 
            this.peso.AutoSize = true;
            this.peso.Location = new System.Drawing.Point(17, 69);
            this.peso.Name = "peso";
            this.peso.Size = new System.Drawing.Size(56, 13);
            this.peso.TabIndex = 51;
            this.peso.Text = "Peso (lbs):";
            // 
            // rpBaja
            // 
            this.rpBaja.AutoSize = true;
            this.rpBaja.Location = new System.Drawing.Point(17, 19);
            this.rpBaja.Name = "rpBaja";
            this.rpBaja.Size = new System.Drawing.Size(25, 13);
            this.rpBaja.TabIndex = 50;
            this.rpBaja.Text = "RP:";
            // 
            // dtfecha
            // 
            this.dtfecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtfecha.Location = new System.Drawing.Point(115, 110);
            this.dtfecha.Name = "dtfecha";
            this.dtfecha.Size = new System.Drawing.Size(129, 20);
            this.dtfecha.TabIndex = 70;
            // 
            // btn_editar
            // 
            this.btn_editar.Enabled = false;
            this.btn_editar.Location = new System.Drawing.Point(75, 173);
            this.btn_editar.Name = "btn_editar";
            this.btn_editar.Size = new System.Drawing.Size(92, 40);
            this.btn_editar.TabIndex = 71;
            this.btn_editar.Text = "Editar";
            this.btn_editar.UseVisualStyleBackColor = true;
            this.btn_editar.Visible = false;
            this.btn_editar.Click += new System.EventHandler(this.btn_editar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(330, 119);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 72;
            this.label1.Text = "ID:";
            this.label1.Visible = false;
            // 
            // txtID
            // 
            this.txtID.Enabled = false;
            this.txtID.Location = new System.Drawing.Point(418, 116);
            this.txtID.MaxLength = 5;
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(100, 20);
            this.txtID.TabIndex = 73;
            this.txtID.Visible = false;
            this.txtID.TextChanged += new System.EventHandler(this.txtID_TextChanged);
            // 
            // cmbRP
            // 
            this.cmbRP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRP.FormattingEnabled = true;
            this.cmbRP.Items.AddRange(new object[] {
            "Muerte",
            "Venta"});
            this.cmbRP.Location = new System.Drawing.Point(115, 16);
            this.cmbRP.Name = "cmbRP";
            this.cmbRP.Size = new System.Drawing.Size(121, 21);
            this.cmbRP.TabIndex = 75;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 150);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 76;
            this.label2.Text = "Detalle: ";
            // 
            // txtdetalle
            // 
            this.txtdetalle.Location = new System.Drawing.Point(115, 147);
            this.txtdetalle.MaxLength = 5;
            this.txtdetalle.Name = "txtdetalle";
            this.txtdetalle.Size = new System.Drawing.Size(100, 20);
            this.txtdetalle.TabIndex = 77;
            // 
            // IngresoBajas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(553, 240);
            this.Controls.Add(this.txtdetalle);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbRP);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_editar);
            this.Controls.Add(this.dtfecha);
            this.Controls.Add(this.btndescartar);
            this.Controls.Add(this.btnguardar);
            this.Controls.Add(this.cmbtipo);
            this.Controls.Add(this.txtprecio);
            this.Controls.Add(this.txtpeso);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.fechaBaja);
            this.Controls.Add(this.peso);
            this.Controls.Add(this.rpBaja);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "IngresoBajas";
            this.Text = "Ingreso Bajas";
            this.Load += new System.EventHandler(this.IngresoBajas_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btndescartar;
        private System.Windows.Forms.Button btnguardar;
        public System.Windows.Forms.ComboBox cmbtipo;
        public System.Windows.Forms.TextBox txtprecio;
        public System.Windows.Forms.TextBox txtpeso;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label fechaBaja;
        private System.Windows.Forms.Label peso;
        private System.Windows.Forms.Label rpBaja;
        public System.Windows.Forms.DateTimePicker dtfecha;
        private System.Windows.Forms.Button btn_editar;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtdetalle;
        public System.Windows.Forms.ComboBox cmbRP;
    }
}