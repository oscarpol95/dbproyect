﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class ingresosGastos
    {

        public int idgastos { get; set; }
        public string fecha { get; set; }
        public string tipo_rubro { get; set; }
        public string detalle { get; set; }
        public int  cantidad { get; set; }
        public float costo_uni { get; set; }
        public float cargos_adi { get; set; }
        public float descuento { get; set; }
        public string cedula { get; set; }
        

        public ingresosGastos() { }

        public ingresosGastos(int idgastos, string fecha, string tipo_rubro, string detalle, int cantidad, float costo_uni, 
            float cargos_adi,float descuento, string cedula)
        {
            this.idgastos = idgastos;
            this.fecha = fecha;
            this.tipo_rubro = tipo_rubro;
            this.detalle = detalle;
            this.cantidad = cantidad;
            this.costo_uni = costo_uni;
            this.cargos_adi = cargos_adi;
            this.descuento = descuento;
            this.cedula = cedula;
        }
    }
}
