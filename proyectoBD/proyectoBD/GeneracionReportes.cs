﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using MySql.Data.MySqlClient;
namespace WindowsFormsApplication1
{
    public partial class GeneracionReportes : Form
    {
        public GeneracionReportes()
        {
            InitializeComponent();
        }

        private void chkReproduccion_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btnvolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void GeneracionReportes_Load(object sender, EventArgs e)
        {

        }

        private void btnReporte_Click(object sender, EventArgs e)
        {
            if (chkBjas.Checked)
            {
                InformacionBajas bajas = new InformacionBajas();
                capturaTabla(bajas.dataGridView1, "CONSULTA BAJAS.\n\n", "bajas.pdf",1);
                System.Diagnostics.Process.Start("C:\\Users\\Usuario\\Desktop\\3\\proyectoBD\\proyectoBD\\bin\\Debug\\bajas.pdf");
            }
            if (chkReproduccion.Checked)
            {
                InformacionPlanesReproductivoPorVaca planRepro = new InformacionPlanesReproductivoPorVaca();
                capturaTabla(planRepro.dgvplanrepro, "CONSULTA PLANES REPRODUCTIVOS.\n\n", "planRepro.pdf",2);
                System.Diagnostics.Process.Start("C:\\Users\\Usuario\\Desktop\\3\\proyectoBD\\proyectoBD\\bin\\Debug\\planRepro.pdf");
            }
            if (chkSanitario.Checked)
            {

                ConsultaInfoPlanSanitario planSani = new ConsultaInfoPlanSanitario();
                capturaTabla(planSani.dataGridView1, "CONSULTA PLANES SANITARIOS.\n\n", "planSani.pdf",3);
                System.Diagnostics.Process.Start("C:\\Users\\Usuario\\Desktop\\3\\proyectoBD\\proyectoBD\\bin\\Debug\\planSani.pdf");
            }

            if (chkIngresosGastos.Checked)
            {
                ConsultaGastosIngresos gastosIngresos = new ConsultaGastosIngresos();
                capturaTabla(gastosIngresos.dataGridView1, "CONSULTA INGRESOS.\n\n", "ingresos.pdf",4);
                System.Diagnostics.Process.Start("C:\\Users\\Usuario\\Desktop\\3\\proyectoBD\\proyectoBD\\bin\\Debug\\ingresos.pdf");


                capturaTabla(gastosIngresos.dataGridView2, "CONSULTA EGRESOS.\n\n", "egresos.pdf",5);
                System.Diagnostics.Process.Start("C:\\Users\\Usuario\\Desktop\\3\\proyectoBD\\proyectoBD\\bin\\Debug\\egresos.pdf");
            }
            if (chkPesajes.Checked)
            {
                ConsultaInfoPesajeporAnimal peso = new ConsultaInfoPesajeporAnimal();
                capturaTabla(peso.dataGridView1, "CONSULTA PESAJES.\n\n", "pesajes.pdf",6);
                System.Diagnostics.Process.Start("C:\\Users\\Usuario\\Desktop\\3\\proyectoBD\\proyectoBD\\bin\\Debug\\pesajes.pdf");
            }
        }
         public void llenaTabla(PdfPTable table ,int colums, Document doc, MySqlDataReader reader)
         {
            var FontColour = new BaseColor(128, 128, 128);
            while (reader.Read())
            {

                for (int i = 0; i < colums; i++)
                {
                    if (i % 2 == 0)

                        FontColour = new BaseColor(128, 128, 128);
                    else
                        FontColour = new BaseColor(0, 0, 0);


                    var Letra = FontFactory.GetFont("Book Antiqua", 8, FontColour);
                    table.AddCell(new Phrase(reader.GetString(i), Letra));
                }
            }
            doc.Add(table);
             doc.Close();
         }
        public void capturaDatosPlanSani(PdfPTable table, Document doc, int colums)
        {
            
            MySqlCommand cmd = new MySqlCommand("reporte_plan_sanitario", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            string fechaDesde = dateDesde.Value.Year + "/" + dateDesde.Value.Month + "/" + dateDesde.Value.Day;
            string fechaHasta = dateHasta.Value.Year + "/" + dateHasta.Value.Month + "/" + dateHasta.Value.Day;
            cmd.Parameters.Add(new MySqlParameter("fecha_desde", fechaDesde));
            cmd.Parameters.Add(new MySqlParameter("fecha_hasta", fechaHasta));
            MySqlDataReader reader = cmd.ExecuteReader();
            llenaTabla(table, colums, doc, reader);


        }
        public void capturaDatosPesajes(PdfPTable table, Document doc, int colums)
        {

            MySqlCommand cmd = new MySqlCommand("reporte_pesaje", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            string fechaDesde = dateDesde.Value.Year + "/" + dateDesde.Value.Month + "/" + dateDesde.Value.Day;
            string fechaHasta = dateHasta.Value.Year + "/" + dateHasta.Value.Month + "/" + dateHasta.Value.Day;
            cmd.Parameters.Add(new MySqlParameter("fecha_desde", fechaDesde));
            cmd.Parameters.Add(new MySqlParameter("fecha_hasta", fechaHasta));
            MySqlDataReader reader = cmd.ExecuteReader();
            llenaTabla(table, colums, doc, reader);


        }
        public void capturaDatosPlanRepro(PdfPTable table , Document doc,int colums)
        {

            MySqlCommand cmd = new MySqlCommand("reporte_planRepro", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            string fechaDesde = dateDesde.Value.Year + "/" + dateDesde.Value.Month + "/" + dateDesde.Value.Day;
            string fechaHasta = dateHasta.Value.Year + "/" + dateHasta.Value.Month + "/" + dateHasta.Value.Day;
            cmd.Parameters.Add(new MySqlParameter("fecha_desde", fechaDesde));
            cmd.Parameters.Add(new MySqlParameter("fecha_hasta", fechaHasta));
            MySqlDataReader reader = cmd.ExecuteReader();

            llenaTabla(table,colums, doc, reader);

        }
        public void capturaDatosBajas(PdfPTable table, Document doc, int colums)
        {

            MySqlCommand cmd = new MySqlCommand("reporte_bajas", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            string fechaDesde = dateDesde.Value.Year + "/" + dateDesde.Value.Month + "/" + dateDesde.Value.Day;
            string fechaHasta = dateHasta.Value.Year + "/" + dateHasta.Value.Month + "/" + dateHasta.Value.Day;
            cmd.Parameters.Add(new MySqlParameter("fecha_desde", fechaDesde));
            cmd.Parameters.Add(new MySqlParameter("fecha_hasta", fechaHasta));
            MySqlDataReader reader = cmd.ExecuteReader();
            llenaTabla(table, colums, doc, reader);


        }
        public void capturaDatosGastos(PdfPTable table, Document doc, int colums)
        {

            MySqlCommand cmd = new MySqlCommand("reporte_gastos", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            string fechaDesde = dateDesde.Value.Year + "/" + dateDesde.Value.Month + "/" + dateDesde.Value.Day;
            string fechaHasta = dateHasta.Value.Year + "/" + dateHasta.Value.Month + "/" + dateHasta.Value.Day;
            cmd.Parameters.Add(new MySqlParameter("fecha_desde", fechaDesde));
            cmd.Parameters.Add(new MySqlParameter("fecha_hasta", fechaHasta));
            MySqlDataReader reader = cmd.ExecuteReader();
            llenaTabla(table, colums, doc, reader);


        }
        public void capturaDatosIngresos(PdfPTable table, Document doc, int colums)
        {

            MySqlCommand cmd = new MySqlCommand("reporte_ingreso", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            string fechaDesde = dateDesde.Value.Year + "/" + dateDesde.Value.Month + "/" + dateDesde.Value.Day;
            string fechaHasta = dateHasta.Value.Year + "/" + dateHasta.Value.Month + "/" + dateHasta.Value.Day;
            cmd.Parameters.Add(new MySqlParameter("fecha_desde", fechaDesde));
            cmd.Parameters.Add(new MySqlParameter("fecha_hasta", fechaHasta));
            MySqlDataReader reader = cmd.ExecuteReader();
            llenaTabla(table, colums, doc, reader);


        }
        public void capturaTabla(DataGridView d, string mensaje, string nombre,int identificador)
        {
            Document doc = new Document(iTextSharp.text.PageSize.LETTER, 10, 10, 42, 35);
            PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream(nombre, FileMode.Create));
            var FontColour = new BaseColor(0, 0, 255);
            var Letra = FontFactory.GetFont("Times New Roman", 8, FontColour);
            Paragraph p = new Paragraph(mensaje + "DESDE: " + dateDesde.Text + "\nHASTA: " + dateHasta.Text + "\n\n", Letra);
            doc.Open();
            doc.Add(p);
            PdfPTable table = null;
            

            if (identificador != 4)
            {

                d.Columns.Remove("Seleccionar");
                table = new PdfPTable(d.Columns.Count);
            }
            else

                table = new PdfPTable(d.Columns.Count);

            for (int j = 0; j < d.Columns.Count; j++)

            {
                
                table.AddCell(new Phrase(d.Columns[j].HeaderText,Letra));

            }
            if (identificador == 1)
            {
                capturaDatosBajas(table, doc, d.Columns.Count);
                
            }
            if (identificador == 2)
            {
                capturaDatosPlanRepro(table,doc, d.Columns.Count);
               
            }
            if (identificador == 3)
            {
                capturaDatosPlanSani(table, doc, d.Columns.Count);
                
            }
            if (identificador == 4)
            {
                capturaDatosIngresos(table, doc, d.Columns.Count);

            }
            if (identificador == 5)
            {
                capturaDatosGastos(table, doc, d.Columns.Count);

            }
            if (identificador == 6)
            {
                capturaDatosPesajes(table, doc, d.Columns.Count);
               
            }


        }

    }
}
