﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    public partial class IngresoBajas : Form
    {
        
        public IngresoBajas(bool flag)
        {
            InitializeComponent();
            btn_editar.Enabled = false;
            if (flag == true)
            {
                btn_editar.Enabled = flag;
                btn_editar.Visible = true;
                btnguardar.Enabled = false;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private static void SoloNumeros(KeyPressEventArgs e, bool isdecimal)
        {
            String aceptados;
            if (!isdecimal)
            {
                aceptados = "0123456789," + Convert.ToChar(8);
            }
            else
                aceptados = "0123456789." + Convert.ToChar(8);

            if (aceptados.Contains("" + e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
            }
        }
        private void txtrp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void txtpeso_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e, false);
        }

        private void txtprecio_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e, false);
        }
        private void txtdetalle_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void IngresoBajas_Load(object sender, EventArgs e)
        {
            List<String> _lista = new List<String>();
            MySqlCommand cmd = new MySqlCommand("buscar_raza", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                if (reader.GetString(10).Equals("ACTIVO"))
                {
                    _lista.Add(reader.GetString(0));
                }
                
            }
            cmbRP.DataSource = _lista;

        }

        private void txtdetalle_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtrp_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtpeso_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnguardar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(cmbRP.Text)||string.IsNullOrWhiteSpace(dtfecha.Text)
                || string.IsNullOrWhiteSpace(cmbtipo.Text) || string.IsNullOrWhiteSpace(txtpeso.Text) || string.IsNullOrWhiteSpace(txtprecio.Text))

                MessageBox.Show("Campos Obligatorios Vacios!", "Campos Vacios!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {

                baja baja = new baja();
                baja.pesolb = float.Parse(txtpeso.Text);
                baja.fecha= dtfecha.Value.Year + "/" + dtfecha.Value.Month + "/" + dtfecha.Value.Day;
                baja.preciolb = float.Parse(txtprecio.Text);
                baja.tipo = cmbtipo.Text; 
                baja.rpvaca = cmbRP.Text.Trim();
                baja.detalle = txtdetalle.Text;
                
                int resultado = bajaAcciones.Agregar(baja);
                if (resultado > 0)
                {
                    MessageBox.Show("Baja Guardada Con Exito!!", "Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();

                }
                else
                {
                    MessageBox.Show("No se pudo guardar la baja", "Fallo!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void btndescartar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_editar_Click(object sender, EventArgs e)
        {
            baja baja = new baja();
            baja.pesolb = (float)Convert.ToDouble(this.txtpeso.Text);
            baja.fecha = dtfecha.Value.Year + "/" + dtfecha.Value.Month + "/" + dtfecha.Value.Day;
            baja.preciolb = (float)Convert.ToDouble(this.txtprecio.Text);
            baja.tipo = cmbtipo.Text.Trim();
            baja.rpvaca = cmbRP.Text.Trim();
            baja.idbaja = Convert.ToInt32(this.txtID.Text);

            int resultado = bajaAcciones.Actualizar(baja);
            if (resultado > 0)
            {
                MessageBox.Show("Baja editada Con Exito!!", "Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();

            }
            else
            {
                MessageBox.Show("No se pudo editar la baja", "Fallo!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void txtID_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
