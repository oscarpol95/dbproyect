﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    class animalAcciones
    {
        public static int Agregar(animal animal)
        {

            int retorno = 0;
            MySqlCommand cmd = new MySqlCommand("new_animal_oscar", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;

            //MySqlCommand cmd1 = new MySqlCommand("new_edad", coneccionBase.ObtenerConexion());
            //cmd1.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("rp",animal.rp));
            cmd.Parameters.Add(new MySqlParameter("n_arete",animal.n_arete));
            cmd.Parameters.Add(new MySqlParameter("id_area",animal.id_area));
            cmd.Parameters.Add(new MySqlParameter("nom",animal.nombre));
            cmd.Parameters.Add(new MySqlParameter("sexo",animal.sexo));
            cmd.Parameters.Add(new MySqlParameter("fecha_llega",animal.fecha_llega));
            cmd.Parameters.Add(new MySqlParameter("haci_ori", animal.haci_ori));
            cmd.Parameters.Add(new MySqlParameter("propi", animal.propi));
            cmd.Parameters.Add(new MySqlParameter("rp_madre", animal.rp_madre));
            cmd.Parameters.Add(new MySqlParameter("rp_padre", animal.rp_padre));
            cmd.Parameters.Add(new MySqlParameter("estado", animal.estado));
            cmd.Parameters.Add(new MySqlParameter("raza", animal.raza));
            cmd.Parameters.Add(new MySqlParameter("rgd", animal.rgd));
            cmd.Parameters.Add(new MySqlParameter("rgd_padre", animal.rgd_padre));

            //retorno = cmd.ExecuteNonQuery();
            /*cmd1.Parameters.Add(new MySqlParameter("rp_edad", animal.rp));
            cmd1.Parameters.Add(new MySqlParameter("fecha_naci_edad", animal.fecha_llega));
            retorno = cmd.ExecuteNonQuery();
            cmd1.ExecuteNonQuery();*/
            try
            {
                retorno = cmd.ExecuteNonQuery();
            }
            catch (MySql.Data.MySqlClient.MySqlException E)
            {
                
               // MessageBox.Show(E.ErrorCode.ToString(), "RP repetida", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                MessageBox.Show("El RP del animal que ha tratado de ingresar esta repetida!", "RP repetida", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return retorno;
        }

        public static int Eliminar(string inRp)
        {
            int retorno = 0;

            MySqlCommand cmd = new MySqlCommand("borrar_animal", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("inRp", inRp));
            retorno = cmd.ExecuteNonQuery();
            coneccionBase.ObtenerConexion().Close();

            return retorno;
        }


       
    }
}
