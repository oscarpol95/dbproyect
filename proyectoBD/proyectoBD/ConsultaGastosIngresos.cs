﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class ConsultaGastosIngresos : Form
    {
        public ConsultaGastosIngresos()
        {
            InitializeComponent();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            IngresoGastos gasto = new IngresoGastos(false);
            gasto.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            gasto.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void otherForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            dataGridView1.Rows.Clear();
            MySqlCommand cmd1 = new MySqlCommand("buscar_ingreso", coneccionBase.ObtenerConexion());
            cmd1.CommandType = CommandType.StoredProcedure;
            MySqlDataReader reader1 = cmd1.ExecuteReader();
            while (reader1.Read())
                dataGridView1.Rows.Add(reader1.GetString(0), reader1.GetString(1), reader1.GetString(2),
                    reader1.GetString(3), reader1.GetString(4), reader1.GetString(5));

            MySqlCommand cmd = new MySqlCommand("buscar_gastosTotal", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
                dataGridView2.Rows.Add(true, reader.GetString(0), reader.GetString(1), reader.GetString(2),
                    reader.GetString(3), reader.GetString(4), reader.GetString(5), reader.GetString(6), reader.GetString(7)
                    , reader.GetString(8), reader.GetString(9));
            this.Show();
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == 1)
                {
                    IngresoGastos animal = new IngresoGastos(true);
                    animal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                    animal.txtID.Text = Convert.ToString(dataGridView2.CurrentRow.Cells[10].Value);
                    animal.dateTimePicker1.Text = Convert.ToString(dataGridView2.CurrentRow.Cells[1].Value);
                    animal.comboBox1.Text = Convert.ToString(dataGridView2.CurrentRow.Cells[2].Value);
                    animal.txtCantidad.Text = Convert.ToString(dataGridView2.CurrentRow.Cells[4].Value);
                    animal.txtCostoU.Text = Convert.ToString(dataGridView2.CurrentRow.Cells[5].Value);
                    animal.txtCargos.Text = Convert.ToString(dataGridView2.CurrentRow.Cells[7].Value);
                    animal.txtDescuento.Text = Convert.ToString(dataGridView2.CurrentRow.Cells[8].Value);
                    animal.Show();
                    this.Hide();
                }
                else if (e.ColumnIndex == 2 && Convert.ToString(dataGridView2.CurrentRow.Cells[2].Value)=="PERSONAL")
                {
                    InformacionPersonal animal = new InformacionPersonal();
                    animal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                    animal.Show();
                    this.Hide();
                }
            }
        }

        private void ConsultaGastosIngresos_Load(object sender, EventArgs e)
        {
            MySqlCommand cmd = new MySqlCommand("buscar_gastosTotal", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            MySqlDataReader reader = cmd.ExecuteReader();
            
            while (reader.Read())
                dataGridView2.Rows.Add( false, reader.GetString(0), reader.GetString(1), reader.GetString(2),
                    reader.GetString(3), reader.GetString(4), reader.GetString(5), reader.GetString(6), reader.GetString(7)
                    , reader.GetString(8));
           
            MySqlCommand cmd1 = new MySqlCommand("buscar_ingreso", coneccionBase.ObtenerConexion());
            cmd1.CommandType = CommandType.StoredProcedure;
            MySqlDataReader reader1 = cmd1.ExecuteReader();
            while (reader1.Read())
                dataGridView1.Rows.Add(reader1.GetString(0), reader1.GetString(1), reader1.GetString(2),
                    reader1.GetString(3), reader1.GetString(4), reader1.GetString(5));

           /* List<String> _lista = new List<String>();
            MySqlCommand cmd2 = new MySqlCommand("buscar_suma_ingresos", coneccionBase.ObtenerConexion());
            cmd2.CommandType = CommandType.StoredProcedure;
            MySqlDataReader reader2 = cmd2.ExecuteReader();
            while (reader2.Read())
            {

                _lista.Add(reader2.GetString(0));

            }
            this.textBox2.Text = _lista[0];

            List<String> _lista1 = new List<String>();
            MySqlCommand cmd3 = new MySqlCommand("buscar_suma_gastos", coneccionBase.ObtenerConexion());
            cmd3.CommandType = CommandType.StoredProcedure;
            MySqlDataReader reader3 = cmd3.ExecuteReader();
            while (reader3.Read())
            {

                _lista1.Add(reader3.GetString(0));

            }
            this.textBox3.Text = _lista1[0];

            int diferencia = Convert.ToInt32(this.textBox2.Text) - Convert.ToInt32(this.textBox3.Text);
            this.textBox1.Text = Convert.ToString(diferencia);*/
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {/*
            if (dataGridView2.SelectedRows.Count == 1)
            {
                string id_Gastos = Convert.ToString(dataGridView2.CurrentRow.Cells[1].Value);
                gastosAcciones.EliminarGastos(id_Gastos);
                dataGridView2.Rows.Remove(dataGridView2.CurrentRow);

                string id_desgloGastos = Convert.ToString(dataGridView2.CurrentRow.Cells[1].Value);
                gastosAcciones.EliminarDesgloseGastos(id_desgloGastos);
                dataGridView2.Rows.Remove(dataGridView2.CurrentRow);

                //POSIBLE ERROR PORQUE LOS EVENTOS DE ELIMINACION SE TRATAN DE FORMA SEPARADA
                //EN UNA MISMA TABLA, POSIBLE SOLUCION UNIR LAS TABLAS Y APLICAR DE NUEVO ELIMINACION

                List<DataGridViewRow> toDelete = new List<DataGridViewRow>();
                foreach (DataGridViewRow row in dvgArea.Rows)
                {
                    if (Convert.ToBoolean(row.Cells[0].Value) == true)
                    {
                        toDelete.Add(row);
                    }
                }
                if (!toDelete.Any())
                    MessageBox.Show("Debe seleccionar una fila");
                else
                {
                    foreach (DataGridViewRow row in toDelete)
                    {
                        string nom = Convert.ToString(row.Cells[1].Value);
                        gastosAcciones.Eliminar(nom);
                        dataGridView2.Rows.Remove(row);
                    }
                }
            }*/
        }

        private void boxIngresos_Enter(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
