﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class IngresoDatosHacienda : Form
    {
        public IngresoDatosHacienda()
        {
            InitializeComponent();
        }
        private static void SoloNumeros(KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }
        private void IngresoDatosHacienda_Load(object sender, EventArgs e)
        {

        }
        private void txtruc_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }
        private void txtnombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar) && e.KeyChar != '\b')
            {
                e.Handled = true;
                MessageBox.Show("Solo se permiten letras", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void txtcipropietario_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }
       
        private void btndescartar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnguardar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtruc.Text) || string.IsNullOrWhiteSpace(txtnombre.Text) || string.IsNullOrWhiteSpace(txtcipropietario.Text) || string.IsNullOrWhiteSpace(txtemail.Text))

                MessageBox.Show("Campos Obligatorios Vacios!", "Campos Vacios!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else if (txtruc.Text.Trim(' ').Length != 13)
            {
                MessageBox.Show("Faltan Caracteres en el campo!", "Faltan Caracteres en el Campo RUC (deben ser 13 caracteres)!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (txtcipropietario.Text.Trim(' ').Length != 10)
            {
                MessageBox.Show("Faltan Caracteres en el campo!", "Faltan Caracteres en el Campo C.I Propietario (deben ser 10 caracteres)!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            /*Validar Mail*/
            else if (!(string.IsNullOrWhiteSpace(txtruc.Text))&& (!IsEmail(txtemail.Text))) {

                MessageBox.Show("Mail no Válido", "Mail ingresado no es válido!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                
            }
            else
            {
                hacienda hacienda = new hacienda();
                hacienda.ruc = txtruc.Text.Trim();
                hacienda.nombre_hac = txtnombre.Text.Trim();
                hacienda.ci_prop = txtcipropietario.Text.Trim();
                hacienda.email = txtemail.Text.Trim();
                int resultado = haciendaAcciones.Agregar(hacienda);
                if (resultado > 0)
                {
                    MessageBox.Show("Datos de Hacienda Guardados Con Exito!!", "Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);


                    this.Close();

                }
                else
                {
                    MessageBox.Show("No se pudo guardar los datos de la Hacienda", "Fallo!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
       
        }


        private void txtruc_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtemail_TextChanged(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Regular expression, which is used to validate an E-Mail address.
        /// </summary>
        public const string MatchEmailPattern =
                  @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
           + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
           + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
           + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";

        /// <summary>
        /// Checks whether the given Email-Parameter is a valid E-Mail address.
        /// </summary>
        /// <param name="email">Parameter-string that contains an E-Mail address.</param>
        /// <returns>True, when Parameter-string is not null and 
        /// contains a valid E-Mail address;
        /// otherwise false.</returns>
        public static bool IsEmail(string email)
        {
            if (email != null) return System.Text.RegularExpressions.Regex.IsMatch(email, MatchEmailPattern);
            else return false;
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtruc.Text) || string.IsNullOrWhiteSpace(txtnombre.Text) || string.IsNullOrWhiteSpace(txtcipropietario.Text) || string.IsNullOrWhiteSpace(txtemail.Text))

                MessageBox.Show("Campos Obligatorios Vacios!", "Campos Vacios!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else if (txtruc.Text.Trim(' ').Length != 13)
            {
                MessageBox.Show("Faltan Caracteres en el campo!", "Faltan Caracteres en el Campo RUC (deben ser 13 caracteres)!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (txtcipropietario.Text.Trim(' ').Length != 10)
            {
                MessageBox.Show("Faltan Caracteres en el campo!", "Faltan Caracteres en el Campo C.I Propietario (deben ser 10 caracteres)!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (!(string.IsNullOrWhiteSpace(txtruc.Text)) && (!IsEmail(txtemail.Text)))
            {

                MessageBox.Show("Mail no Válido", "Mail ingresado no es válido!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
            else
            {
                hacienda hacienda = new hacienda();
                hacienda.ruc = txtruc.Text.Trim();
                hacienda.nombre_hac = txtnombre.Text.Trim();
                hacienda.ci_prop = txtcipropietario.Text.Trim();
                hacienda.email = txtemail.Text.Trim();
                hacienda.cod = Convert.ToInt32(txtID.Text);
                int resultado = haciendaAcciones.Actualizar(hacienda);
                if (resultado > 0)
                {
                    MessageBox.Show("Datos de Hacienda Editados Con Exito!!", "Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);


                    this.Close();

                }
                else
                {
                    MessageBox.Show("No se pudo guardar los datos de la Hacienda", "Fallo!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
        }
    }
}
    
