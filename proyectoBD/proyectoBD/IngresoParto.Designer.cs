﻿namespace WindowsFormsApplication1
{
    partial class IngresoParto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRPPadre = new System.Windows.Forms.Label();
            this.lblSexoTernero = new System.Windows.Forms.Label();
            this.lblRPMadre = new System.Windows.Forms.Label();
            this.lblEstado = new System.Windows.Forms.Label();
            this.lblFechaParto = new System.Windows.Forms.Label();
            this.txtRPPadre = new System.Windows.Forms.TextBox();
            this.txtRPMadre = new System.Windows.Forms.TextBox();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnDescartar = new System.Windows.Forms.Button();
            this.cmbSexo = new System.Windows.Forms.ComboBox();
            this.cmbEstado = new System.Windows.Forms.ComboBox();
            this.dateParto = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // lblRPPadre
            // 
            this.lblRPPadre.AutoSize = true;
            this.lblRPPadre.BackColor = System.Drawing.SystemColors.Control;
            this.lblRPPadre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRPPadre.Location = new System.Drawing.Point(63, 61);
            this.lblRPPadre.Name = "lblRPPadre";
            this.lblRPPadre.Size = new System.Drawing.Size(56, 13);
            this.lblRPPadre.TabIndex = 0;
            this.lblRPPadre.Text = "RP Padre:";
            this.lblRPPadre.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblSexoTernero
            // 
            this.lblSexoTernero.AutoSize = true;
            this.lblSexoTernero.BackColor = System.Drawing.SystemColors.Control;
            this.lblSexoTernero.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSexoTernero.Location = new System.Drawing.Point(63, 87);
            this.lblSexoTernero.Name = "lblSexoTernero";
            this.lblSexoTernero.Size = new System.Drawing.Size(74, 13);
            this.lblSexoTernero.TabIndex = 1;
            this.lblSexoTernero.Text = "Sexo Ternero:";
            // 
            // lblRPMadre
            // 
            this.lblRPMadre.AutoSize = true;
            this.lblRPMadre.BackColor = System.Drawing.SystemColors.Control;
            this.lblRPMadre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRPMadre.Location = new System.Drawing.Point(269, 61);
            this.lblRPMadre.Name = "lblRPMadre";
            this.lblRPMadre.Size = new System.Drawing.Size(58, 13);
            this.lblRPMadre.TabIndex = 2;
            this.lblRPMadre.Text = "RP Madre:";
            // 
            // lblEstado
            // 
            this.lblEstado.AutoSize = true;
            this.lblEstado.BackColor = System.Drawing.SystemColors.Control;
            this.lblEstado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstado.Location = new System.Drawing.Point(269, 87);
            this.lblEstado.Name = "lblEstado";
            this.lblEstado.Size = new System.Drawing.Size(43, 13);
            this.lblEstado.TabIndex = 3;
            this.lblEstado.Text = "Estado:";
            // 
            // lblFechaParto
            // 
            this.lblFechaParto.AutoSize = true;
            this.lblFechaParto.BackColor = System.Drawing.SystemColors.Control;
            this.lblFechaParto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaParto.Location = new System.Drawing.Point(63, 130);
            this.lblFechaParto.Name = "lblFechaParto";
            this.lblFechaParto.Size = new System.Drawing.Size(83, 13);
            this.lblFechaParto.TabIndex = 4;
            this.lblFechaParto.Text = "Fecha de Parto:";
            // 
            // txtRPPadre
            // 
            this.txtRPPadre.Location = new System.Drawing.Point(163, 58);
            this.txtRPPadre.MaxLength = 3;
            this.txtRPPadre.Name = "txtRPPadre";
            this.txtRPPadre.Size = new System.Drawing.Size(100, 20);
            this.txtRPPadre.TabIndex = 5;
            this.txtRPPadre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRPPadre_KeyPress);
            // 
            // txtRPMadre
            // 
            this.txtRPMadre.Location = new System.Drawing.Point(346, 58);
            this.txtRPMadre.MaxLength = 3;
            this.txtRPMadre.Name = "txtRPMadre";
            this.txtRPMadre.ReadOnly = true;
            this.txtRPMadre.Size = new System.Drawing.Size(100, 20);
            this.txtRPMadre.TabIndex = 7;
            this.txtRPMadre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRPMadre_KeyPress);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(93, 188);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(94, 33);
            this.btnGuardar.TabIndex = 11;
            this.btnGuardar.Text = "Guardar/Cerrar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnDescartar
            // 
            this.btnDescartar.Location = new System.Drawing.Point(322, 188);
            this.btnDescartar.Name = "btnDescartar";
            this.btnDescartar.Size = new System.Drawing.Size(89, 33);
            this.btnDescartar.TabIndex = 12;
            this.btnDescartar.Text = "Descartar";
            this.btnDescartar.UseVisualStyleBackColor = true;
            this.btnDescartar.Click += new System.EventHandler(this.btnDescartar_Click);
            // 
            // cmbSexo
            // 
            this.cmbSexo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSexo.FormattingEnabled = true;
            this.cmbSexo.Items.AddRange(new object[] {
            "Macho",
            "Hembra"});
            this.cmbSexo.Location = new System.Drawing.Point(163, 86);
            this.cmbSexo.Name = "cmbSexo";
            this.cmbSexo.Size = new System.Drawing.Size(100, 21);
            this.cmbSexo.TabIndex = 13;
            // 
            // cmbEstado
            // 
            this.cmbEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEstado.FormattingEnabled = true;
            this.cmbEstado.Items.AddRange(new object[] {
            "Vivo",
            "Muerto"});
            this.cmbEstado.Location = new System.Drawing.Point(346, 83);
            this.cmbEstado.Name = "cmbEstado";
            this.cmbEstado.Size = new System.Drawing.Size(100, 21);
            this.cmbEstado.TabIndex = 14;
            // 
            // dateParto
            // 
            this.dateParto.Location = new System.Drawing.Point(174, 126);
            this.dateParto.Name = "dateParto";
            this.dateParto.Size = new System.Drawing.Size(200, 20);
            this.dateParto.TabIndex = 15;
            this.dateParto.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // IngresoParto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(552, 249);
            this.Controls.Add(this.dateParto);
            this.Controls.Add(this.cmbEstado);
            this.Controls.Add(this.cmbSexo);
            this.Controls.Add(this.btnDescartar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.txtRPMadre);
            this.Controls.Add(this.txtRPPadre);
            this.Controls.Add(this.lblFechaParto);
            this.Controls.Add(this.lblEstado);
            this.Controls.Add(this.lblRPMadre);
            this.Controls.Add(this.lblSexoTernero);
            this.Controls.Add(this.lblRPPadre);
            this.Name = "IngresoParto";
            this.Text = "Ingreso de Partos";
            this.Load += new System.EventHandler(this.IngresoParto_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRPPadre;
        private System.Windows.Forms.Label lblSexoTernero;
        private System.Windows.Forms.Label lblRPMadre;
        private System.Windows.Forms.Label lblEstado;
        private System.Windows.Forms.Label lblFechaParto;
        public System.Windows.Forms.TextBox txtRPPadre;
        public System.Windows.Forms.TextBox txtRPMadre;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnDescartar;
        public System.Windows.Forms.ComboBox cmbSexo;
        public System.Windows.Forms.ComboBox cmbEstado;
        public System.Windows.Forms.DateTimePicker dateParto;
    }
}