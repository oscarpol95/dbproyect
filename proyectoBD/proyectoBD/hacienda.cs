﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class hacienda
    {
        public string ruc { get; set; }
        public string nombre_hac { get; set; }
        public string ci_prop { get; set; }
        public string email { get; set; }
        public int cod { get; set; }
        public hacienda() { }

        public hacienda(string ruc, string nombre_hac, string ci_prop, string email, int id)
        {
            this.ruc = ruc;
            this.nombre_hac = nombre_hac;
            this.ci_prop = ci_prop;
            this.email = email;
            this.cod = id;
        }
    }
}
