﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class baja
    {
        public int idbaja { get; set; }
        public string fecha { get; set; }
        public string tipo { get; set; }
        public float pesolb { get; set; }
        public float preciolb { get; set; }
        public string rpvaca { get; set; }
        public string detalle { get; set; }
        
        public baja() { }

        public baja( int idbaja, string fecha, string tipo, float peso, float precio, string rpvaca, string detalle)
        {
            this.idbaja = idbaja;
            this.fecha = fecha;
            this.tipo = tipo;
            this.pesolb = peso;
            this.preciolb= precio;
            this.rpvaca = rpvaca;
            this.detalle = detalle;
        }
    }
}
