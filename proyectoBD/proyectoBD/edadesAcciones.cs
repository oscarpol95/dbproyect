﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    class edadesAcciones
    {
        public static int Agregar(edades edad)
        {

            int retorno = 0;
            MySqlCommand cmd = new MySqlCommand("new_edad", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new MySqlParameter("rp_edad", edad.rp));
            cmd.Parameters.Add(new MySqlParameter("fecha_naci_edad", edad.fecha_naci));

            //retorno = cmd.ExecuteNonQuery();
            try
            {
                retorno = cmd.ExecuteNonQuery();
            }
            catch (MySql.Data.MySqlClient.MySqlException e)
            {

               // MessageBox.Show(E.ErrorCode.ToString(), "RP repetida", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                MessageBox.Show("El RP del animal que ha tratado de ingresar esta repetida!", "RP repetida", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return retorno;
            
        }

    }
}
