﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;

namespace WindowsFormsApplication1
{
    class medicamentoAcciones
    {
        
        public static int Agregar(medicamento medicamento)
        {

            int retorno = 0;
            MySqlCommand cmd = new MySqlCommand("new_medicamento", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("nombre", medicamento.nombre));
            cmd.Parameters.Add(new MySqlParameter("tipo", medicamento.tipo));
            cmd.Parameters.Add(new MySqlParameter("periodicidad", medicamento.periodicidad));
            cmd.Parameters.Add(new MySqlParameter("posologia", medicamento.posologia));
            retorno = cmd.ExecuteNonQuery();

            return retorno;
        }
    }
}
