﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class medicamento
    {
        public string nombre { get; set; }
        public string tipo { get; set; }
        public string periodicidad { get; set; }
        public string posologia { get; set; }
       
        public medicamento() { }

        public medicamento(string nombre, string tipo, string periodicidad, string posologia)
        {
            this.nombre = nombre;
            this.tipo = tipo;
            this.periodicidad = periodicidad;
            this.posologia = posologia;
            
        }
    }
}

