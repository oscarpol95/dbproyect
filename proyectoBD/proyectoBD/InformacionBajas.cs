﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    public partial class InformacionBajas : Form
    {
        public InformacionBajas()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            dataGridView1.Rows.Clear();
            string criterio = rdcriterio.Text.Trim();
            MySqlCommand cmd = null;

            if (criterio == "")
            {
                cmd = new MySqlCommand("buscar_bajas", coneccionBase.ObtenerConexion());
            }
            else if (rdrp.Checked == true)
            {
                using (cmd = new MySqlCommand("buscar_bajas_rp", coneccionBase.ObtenerConexion()))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@clave", criterio);
                }
            }
            else if (rdnombre.Checked == true)
            {
                using (cmd = new MySqlCommand("buscar_bajas_nombre", coneccionBase.ObtenerConexion()))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@nombre", criterio);
                }
            }
            else if (rdpropietario.Checked == true)
            {
                using (cmd = new MySqlCommand("buscar_bajas_propietario", coneccionBase.ObtenerConexion()))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@propietario", criterio);
                }
            }
            else if (rdtipo.Checked == true)
            {
                using (cmd = new MySqlCommand("buscar_bajas_tipo", coneccionBase.ObtenerConexion()))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@tipo", criterio);
                }
            }
            else
            {
                cmd = new MySqlCommand("buscar_bajas", coneccionBase.ObtenerConexion());

            }
            cmd.CommandType = CommandType.StoredProcedure;

            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
             
                dataGridView1.Rows.Add(false, reader.GetString(0), reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4));
               

            }
           
        }

        private void InformacionBajas_Load(object sender, EventArgs e)
        {
            MySqlCommand cmd = new MySqlCommand("buscar_bajas", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
                dataGridView1.Rows.Add(true, reader.GetString(0), reader.GetString(5), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4));
        }

        private void lblNuevo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            WindowsFormsApplication1.IngresoBajas bajas = new WindowsFormsApplication1.IngresoBajas(false);
            bajas.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            bajas.Show();
            this.Hide();
        }

        void otherForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            dataGridView1.Rows.Clear();
            MySqlCommand cmd = new MySqlCommand("buscar_bajas", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
                dataGridView1.Rows.Add(true, reader.GetString(0), reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4));
            this.Show();
        }

        private void btnvolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2 && e.RowIndex >= 0)
            {
                WindowsFormsApplication1.IngresoBajas animal = new WindowsFormsApplication1.IngresoBajas(true);
                animal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                animal.cmbRP.Text = Convert.ToString(dataGridView1.CurrentRow.Cells[2].Value);
                animal.txtID.Text = Convert.ToString(dataGridView1.CurrentRow.Cells[1].Value);
                animal.dtfecha.Text = Convert.ToString(dataGridView1.CurrentRow.Cells[3].Value);
                animal.txtpeso.Text = Convert.ToString(dataGridView1.CurrentRow.Cells[5].Value);
                animal.txtprecio.Text = Convert.ToString(dataGridView1.CurrentRow.Cells[6].Value);
                animal.cmbtipo.Text = Convert.ToString(dataGridView1.CurrentRow.Cells[4].Value);
                animal.cmbRP.Enabled = false;
                animal.Show();
                this.Hide();
            }
        }

        private void lblEliminar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            List<DataGridViewRow> toDelete = new List<DataGridViewRow>();
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (Convert.ToBoolean(row.Cells[0].Value) == true)
                {
                    toDelete.Add(row);
                }
            }
            if (!toDelete.Any())
                MessageBox.Show("Debe seleccionar una fila");
            else
            {
                foreach (DataGridViewRow row in toDelete)
                {
                    string id = Convert.ToString(row.Cells[1].Value);
                    string rp = Convert.ToString(row.Cells[2].Value);
                    bajaAcciones.Eliminar(id, rp);
                    dataGridView1.Rows.Remove(row);
                }
            }
        }

        private void Mostrar_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            MySqlCommand cmd = new MySqlCommand("buscar_bajasFecha", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.Add(new MySqlParameter("rp", txtRP.Text));
            string fechaDesde = dateTimePicker1.Value.Year + "/" + dateTimePicker1.Value.Month + "/" + dateTimePicker1.Value.Day;
            string fechaHasta = dateTimePicker2.Value.Year + "/" + dateTimePicker2.Value.Month + "/" + dateTimePicker1.Value.Day;
            cmd.Parameters.Add(new MySqlParameter("fecha_desde", fechaDesde));
            cmd.Parameters.Add(new MySqlParameter("fecha_hasta", fechaHasta));
            MySqlDataReader reader = cmd.ExecuteReader();
   
                

            while (reader.Read())
                dataGridView1.Rows.Add(false, reader.GetString(0), reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4));
               // dataGridView1.Rows.Add(false, reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5));

        }
    }
}
