﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;

namespace WindowsFormsApplication1
{
    class haciendaAcciones
    {
        public static int Agregar(hacienda hacienda)
        {

            int retorno = 0;
            MySqlCommand cmd = new MySqlCommand("new_hacienda", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.Add(new MySqlParameter("ruc", hacienda.ruc));
            cmd.Parameters.Add(new MySqlParameter("nom", hacienda.nombre_hac));
            cmd.Parameters.Add(new MySqlParameter("ci_prop", hacienda.ci_prop));
            cmd.Parameters.Add(new MySqlParameter("email", hacienda.email));
            cmd.Parameters.Add(new MySqlParameter("id", null));


            retorno = cmd.ExecuteNonQuery();

            return retorno;
        }
        public static int Actualizar(hacienda hacienda)
        {

            int retorno = 0;
            MySqlCommand cmd = new MySqlCommand("update_hacienda", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.Add(new MySqlParameter("ruc", hacienda.ruc));
            cmd.Parameters.Add(new MySqlParameter("nom", hacienda.nombre_hac));
            cmd.Parameters.Add(new MySqlParameter("ci_prop", hacienda.ci_prop));
            cmd.Parameters.Add(new MySqlParameter("email", hacienda.email));
            cmd.Parameters.Add(new MySqlParameter("id", hacienda.cod));


            retorno = cmd.ExecuteNonQuery();

            return retorno;
        }

    }
}
