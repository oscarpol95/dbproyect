﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class IngresoParto : Form
    {
        public IngresoParto()
        {
            InitializeComponent();
        }

        private void IngresoParto_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private static void SoloNumeros(KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void txtRPPadre_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
             if (string.IsNullOrWhiteSpace(txtRPMadre.Text)||string.IsNullOrWhiteSpace(txtRPPadre.Text)||string.IsNullOrWhiteSpace(cmbSexo.Text)||string.IsNullOrWhiteSpace(cmbEstado.Text)||string.IsNullOrWhiteSpace(dateParto.Text))

                MessageBox.Show("Campos Obligatorios Vacios!", "Campos Vacios!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else 

            {
                parto parto = new parto();
                parto.rp_padre = txtRPPadre.Text.Trim();
                parto.fecha_parto = dateParto.Value.Year + "/" + dateParto.Value.Month + "/" + dateParto.Value.Day;
                parto.sexo = cmbSexo.Text.Trim();
                parto.estado = cmbEstado.Text.Trim();
                parto.rp_vaca = txtRPMadre.Text.Trim();
                if (parto.estado == "Muerto")
                {
                    int resultado = partoAcciones.Agregar(parto);
                    if (resultado>0)
                    {
                        MessageBox.Show("Parto Guardado Con Exito!!", "Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo guardar el parto", "Fallo!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }

                }
                else
                {
                    IngresoInfoAnimalParido animal = new IngresoInfoAnimalParido();
                    animal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                    animal.txtMadre.Text = this.txtRPMadre.Text;
                    animal.txtPadre.Text = this.txtRPPadre.Text;
                    animal.txtSexo.Text = this.cmbSexo.Text.Trim();
                    animal.txtFechaNacimiento.Text = parto.fecha_parto;
                    animal.Show();
                    this.Hide();
                }
            }

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        void otherForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }

        private void btnDescartar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtRPMadre_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }
    }
}
