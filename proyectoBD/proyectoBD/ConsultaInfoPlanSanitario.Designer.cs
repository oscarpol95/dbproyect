﻿namespace WindowsFormsApplication1
{
    partial class ConsultaInfoPlanSanitario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnVolver = new System.Windows.Forms.Button();
            this.lblNuevo = new System.Windows.Forms.LinkLabel();
            this.lblEliminar = new System.Windows.Forms.LinkLabel();
            this.dateHasta = new System.Windows.Forms.DateTimePicker();
            this.dateDesde = new System.Windows.Forms.DateTimePicker();
            this.txtRP = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.lblAniohasta = new System.Windows.Forms.Label();
            this.lblAnioDesde = new System.Windows.Forms.Label();
            this.lblRP = new System.Windows.Forms.Label();
            this.btnMostrar = new System.Windows.Forms.Button();
            this.Seleccionar = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.IDPlan = new System.Windows.Forms.DataGridViewLinkColumn();
            this.NombrePlan = new System.Windows.Forms.DataGridViewLinkColumn();
            this.FechaAplicacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoPlan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NombreMedicamento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Posología = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Periodicidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dosis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnVolver
            // 
            this.btnVolver.Location = new System.Drawing.Point(478, 303);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(153, 34);
            this.btnVolver.TabIndex = 23;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // lblNuevo
            // 
            this.lblNuevo.AutoSize = true;
            this.lblNuevo.Location = new System.Drawing.Point(867, 283);
            this.lblNuevo.Name = "lblNuevo";
            this.lblNuevo.Size = new System.Drawing.Size(54, 13);
            this.lblNuevo.TabIndex = 22;
            this.lblNuevo.TabStop = true;
            this.lblNuevo.Text = "Nuevo (+)";
            this.lblNuevo.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblNuevo_LinkClicked);
            // 
            // lblEliminar
            // 
            this.lblEliminar.AutoSize = true;
            this.lblEliminar.Location = new System.Drawing.Point(73, 283);
            this.lblEliminar.Name = "lblEliminar";
            this.lblEliminar.Size = new System.Drawing.Size(55, 13);
            this.lblEliminar.TabIndex = 21;
            this.lblEliminar.TabStop = true;
            this.lblEliminar.Text = "Eliminar (-)";
            this.lblEliminar.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblEliminar_LinkClicked);
            // 
            // dateHasta
            // 
            this.dateHasta.Location = new System.Drawing.Point(632, 71);
            this.dateHasta.Name = "dateHasta";
            this.dateHasta.Size = new System.Drawing.Size(200, 20);
            this.dateHasta.TabIndex = 20;
            // 
            // dateDesde
            // 
            this.dateDesde.Location = new System.Drawing.Point(252, 71);
            this.dateDesde.Name = "dateDesde";
            this.dateDesde.Size = new System.Drawing.Size(200, 20);
            this.dateDesde.TabIndex = 19;
            // 
            // txtRP
            // 
            this.txtRP.Location = new System.Drawing.Point(252, 35);
            this.txtRP.Name = "txtRP";
            this.txtRP.ReadOnly = true;
            this.txtRP.Size = new System.Drawing.Size(100, 20);
            this.txtRP.TabIndex = 17;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Seleccionar,
            this.IDPlan,
            this.NombrePlan,
            this.FechaAplicacion,
            this.TipoPlan,
            this.NombreMedicamento,
            this.Posología,
            this.Periodicidad,
            this.Dosis});
            this.dataGridView1.Location = new System.Drawing.Point(76, 130);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(845, 150);
            this.dataGridView1.TabIndex = 16;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // lblAniohasta
            // 
            this.lblAniohasta.AutoSize = true;
            this.lblAniohasta.Location = new System.Drawing.Point(567, 77);
            this.lblAniohasta.Name = "lblAniohasta";
            this.lblAniohasta.Size = new System.Drawing.Size(38, 13);
            this.lblAniohasta.TabIndex = 15;
            this.lblAniohasta.Text = "Hasta:";
            // 
            // lblAnioDesde
            // 
            this.lblAnioDesde.AutoSize = true;
            this.lblAnioDesde.Location = new System.Drawing.Point(182, 77);
            this.lblAnioDesde.Name = "lblAnioDesde";
            this.lblAnioDesde.Size = new System.Drawing.Size(41, 13);
            this.lblAnioDesde.TabIndex = 14;
            this.lblAnioDesde.Text = "Desde:";
            // 
            // lblRP
            // 
            this.lblRP.AutoSize = true;
            this.lblRP.Location = new System.Drawing.Point(182, 38);
            this.lblRP.Name = "lblRP";
            this.lblRP.Size = new System.Drawing.Size(25, 13);
            this.lblRP.TabIndex = 12;
            this.lblRP.Text = "RP:";
            // 
            // btnMostrar
            // 
            this.btnMostrar.Location = new System.Drawing.Point(856, 68);
            this.btnMostrar.Name = "btnMostrar";
            this.btnMostrar.Size = new System.Drawing.Size(75, 23);
            this.btnMostrar.TabIndex = 25;
            this.btnMostrar.Text = "Mostrar";
            this.btnMostrar.UseVisualStyleBackColor = true;
            this.btnMostrar.Click += new System.EventHandler(this.btnMostrar_Click);
            // 
            // Seleccionar
            // 
            this.Seleccionar.HeaderText = "Seleccionar";
            this.Seleccionar.Name = "Seleccionar";
            this.Seleccionar.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Seleccionar.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // IDPlan
            // 
            this.IDPlan.HeaderText = "Codigo";
            this.IDPlan.Name = "IDPlan";
            this.IDPlan.ReadOnly = true;
            this.IDPlan.Visible = false;
            // 
            // NombrePlan
            // 
            this.NombrePlan.HeaderText = "Nombre Plan";
            this.NombrePlan.Name = "NombrePlan";
            this.NombrePlan.ReadOnly = true;
            this.NombrePlan.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.NombrePlan.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // FechaAplicacion
            // 
            this.FechaAplicacion.HeaderText = "Fecha Aplicación";
            this.FechaAplicacion.Name = "FechaAplicacion";
            this.FechaAplicacion.ReadOnly = true;
            // 
            // TipoPlan
            // 
            this.TipoPlan.HeaderText = "Tipo Plan";
            this.TipoPlan.Name = "TipoPlan";
            this.TipoPlan.ReadOnly = true;
            // 
            // NombreMedicamento
            // 
            this.NombreMedicamento.HeaderText = "Nombre Medicamento";
            this.NombreMedicamento.Name = "NombreMedicamento";
            this.NombreMedicamento.ReadOnly = true;
            // 
            // Posología
            // 
            this.Posología.HeaderText = "Posología Medicamento";
            this.Posología.Name = "Posología";
            this.Posología.ReadOnly = true;
            // 
            // Periodicidad
            // 
            this.Periodicidad.HeaderText = "Periodicidad Medicamento";
            this.Periodicidad.Name = "Periodicidad";
            this.Periodicidad.ReadOnly = true;
            // 
            // Dosis
            // 
            this.Dosis.HeaderText = "Dosis";
            this.Dosis.Name = "Dosis";
            this.Dosis.ReadOnly = true;
            // 
            // ConsultaInfoPlanSanitario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(988, 370);
            this.Controls.Add(this.btnMostrar);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.lblNuevo);
            this.Controls.Add(this.lblEliminar);
            this.Controls.Add(this.dateHasta);
            this.Controls.Add(this.dateDesde);
            this.Controls.Add(this.txtRP);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.lblAniohasta);
            this.Controls.Add(this.lblAnioDesde);
            this.Controls.Add(this.lblRP);
            this.Name = "ConsultaInfoPlanSanitario";
            this.Text = "Consulta de Información de Plan Sanitario por Animal";
            this.Load += new System.EventHandler(this.ConsultaInfoPlanSanitario_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnVolver;
        private System.Windows.Forms.LinkLabel lblNuevo;
        private System.Windows.Forms.LinkLabel lblEliminar;
        private System.Windows.Forms.DateTimePicker dateHasta;
        private System.Windows.Forms.DateTimePicker dateDesde;
        public System.Windows.Forms.TextBox txtRP;
        private System.Windows.Forms.Label lblAniohasta;
        private System.Windows.Forms.Label lblAnioDesde;
        private System.Windows.Forms.Label lblRP;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnMostrar;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Seleccionar;
        private System.Windows.Forms.DataGridViewLinkColumn IDPlan;
        private System.Windows.Forms.DataGridViewLinkColumn NombrePlan;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaAplicacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoPlan;
        private System.Windows.Forms.DataGridViewTextBoxColumn NombreMedicamento;
        private System.Windows.Forms.DataGridViewTextBoxColumn Posología;
        private System.Windows.Forms.DataGridViewTextBoxColumn Periodicidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dosis;
    }
}