﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class pesaje
    {
        public string id_pesaje { get; set; }
        public string rp_vaca { get; set; }
        public float peso { get; set; }
        public string fecha_pesaje { get; set; }
        
        public pesaje() { }

        public pesaje(string id_pesaje, string rp_vaca,float peso,string fecha_pesaje)
        {
            this.id_pesaje = id_pesaje;
            this.rp_vaca = rp_vaca;
            this.peso = peso;
            this.fecha_pesaje = fecha_pesaje;
            
        }
    }
}
