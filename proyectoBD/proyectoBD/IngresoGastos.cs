﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    public partial class IngresoGastos : Form
    {
        public IngresoGastos(bool flag)
        {
            InitializeComponent();
            btnEditar.Enabled = false;
            if (flag == true)
            {
                btnEditar.Enabled = flag;
                btnEditar.Visible = true;
                btnGuardar.Enabled = false;
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtCantidad.Text) || string.IsNullOrWhiteSpace(txtCostoU.Text) ||
                string.IsNullOrWhiteSpace(txtDescuento.Text) || string.IsNullOrWhiteSpace(dateTimePicker1.Text))
                //si el tipo de gasto es personal hacer que la cedula sea obligatoria
                MessageBox.Show("Campos Obligatorios Vacios!", "Campos Vacios!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                ingresosGastos gastos = new ingresosGastos();
                gastos.tipo_rubro = comboBox1.Text.Trim();
                gastos.detalle = textBox2.Text.Trim();
                gastos.fecha = dateTimePicker1.Value.Year + "/" + dateTimePicker1.Value.Month + "/" + dateTimePicker1.Value.Day;
                gastos.cantidad = Convert.ToInt32(txtCantidad.Text);
                gastos.costo_uni = (float)Convert.ToDouble(txtCostoU.Text);
                gastos.cargos_adi = (float)Convert.ToDouble(txtCargos.Text);
                gastos.descuento = (float)Convert.ToDouble(txtDescuento.Text);
                gastos.cedula = null;

                if (comboBox1.Text.Equals("PERSONAL"))
                {

                    gastos.cedula = cmbCed.Text;
                    if (gastos.cedula.Equals(null))
                    {
                        MessageBox.Show("Selecciona una cedula", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    int resultado = ingresosGastosAcciones.AgregarGasto(gastos);
                    if (resultado > 0)
                    {
                        MessageBox.Show("Operación exitosa!!", "Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo guardar Gasto", "Fallo!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }

                }
                else
                {
                    int resultado = ingresosGastosAcciones.AgregarGasto(gastos);
                    if (resultado > 0)
                    {
                        MessageBox.Show("Operación exitosa!!", "Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo guardar el Gasto", "Fallo!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }

                /*int resultado = ingresosGastosAcciones.AgregarGasto(gastos);
                if (resultado > 0)
                {
                    if (comboBox1.Text.Equals("PERSONAL")) {
                        gastos.cedula = cmbCed.Text;
                        int resultado1 = ingresosGastosAcciones.AgregarGastoPersonal(gastos);
                    }
                    
                    MessageBox.Show("Operación exitosa!!", "Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();

                }
                else
                {
                    MessageBox.Show("No se pudo guardar el cliente", "Fallo!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }*/

            }
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private static void SoloNumeros(KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }


        private void txtCedula_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void txtCantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private static void SoloDecimales(KeyPressEventArgs e, bool isdecimal)
        {
            String aceptados;
            if (!isdecimal)
            {
                aceptados = "0123456789," + Convert.ToChar(8);
            }
            else
                aceptados = "0123456789." + Convert.ToChar(8);

            if (aceptados.Contains("" + e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
            }
        }

        private void txtCostoU_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloDecimales(e, false);
        }

        private void txtCargos_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloDecimales(e, false);
        }

        private void txtDescuento_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloDecimales(e, false);
        }

        private void txtCedula_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem.ToString().Trim() == "PERSONAL")
            {
                label2.Visible = true;
                cmbCed.Visible = true;
                cmbCed.Enabled = true;
            }
            else
            {
                label2.Visible = false;
                cmbCed.Visible = false;
                cmbCed.Enabled = false;
            }
        }

        private void IngresoGastos_Load(object sender, EventArgs e)
        {
            List<String> _lista = new List<String>();
            List<String> _listaNombre = new List<String>();
            MySqlCommand cmd = new MySqlCommand("buscar_persona", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                _lista.Add(reader.GetString(0));
                _listaNombre.Add(reader.GetString(1));
            }
            this.cmbCed.DataSource = _lista;
            //txtNombre.Text = _listaNombre[_lista.IndexOf(cmbCed.Text)];


        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtCantidad.Text) || string.IsNullOrWhiteSpace(txtCostoU.Text) ||
                string.IsNullOrWhiteSpace(txtDescuento.Text) || string.IsNullOrWhiteSpace(dateTimePicker1.Text))
                //si el tipo de gasto es personal hacer que la cedula sea obligatoria
                MessageBox.Show("Campos Obligatorios Vacios!", "Campos Vacios!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                ingresosGastos gastos = new ingresosGastos();
                gastos.tipo_rubro = comboBox1.Text.Trim();
                gastos.detalle = textBox2.Text.Trim();
                gastos.fecha = dateTimePicker1.Value.Year + "/" + dateTimePicker1.Value.Month + "/" + dateTimePicker1.Value.Day;
                gastos.cantidad = Convert.ToInt32(txtCantidad.Text);
                gastos.costo_uni = (float)Convert.ToDouble(txtCostoU.Text);
                gastos.cargos_adi = (float)Convert.ToDouble(txtCargos.Text);
                gastos.descuento = (float)Convert.ToDouble(txtDescuento.Text);
                gastos.cedula = null;
                gastos.idgastos = Convert.ToInt32(txtID.Text);

                if (comboBox1.Text.Equals("PERSONAL"))
                {

                    gastos.cedula = cmbCed.Text;
                    if (gastos.cedula.Equals(null))
                    {
                        MessageBox.Show("Selecciona una cedula", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    int resultado = ingresosGastosAcciones.ActualizarGasto(gastos);
                    if (resultado > 0)
                    {
                        MessageBox.Show("Operación exitosa!!", "Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo editar la baja", "Fallo!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }

                }
                else
                {
                    int resultado = ingresosGastosAcciones.AgregarGasto(gastos);
                    if (resultado > 0)
                    {
                        MessageBox.Show("Operación exitosa!!", "Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo editar la baja", "Fallo!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
        }
    }
}
