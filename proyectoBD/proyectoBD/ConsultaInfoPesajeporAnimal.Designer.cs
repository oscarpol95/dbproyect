﻿namespace WindowsFormsApplication1
{
    partial class ConsultaInfoPesajeporAnimal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRP = new System.Windows.Forms.Label();
            this.lblDesde = new System.Windows.Forms.Label();
            this.lblAniohasta = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.txtRP = new System.Windows.Forms.TextBox();
            this.dateDesde = new System.Windows.Forms.DateTimePicker();
            this.dateHasta = new System.Windows.Forms.DateTimePicker();
            this.lblEliminar = new System.Windows.Forms.LinkLabel();
            this.lblNuevo = new System.Windows.Forms.LinkLabel();
            this.btnVolver = new System.Windows.Forms.Button();
            this.btnMostrar = new System.Windows.Forms.Button();
            this.Seleccionar = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.IDPesaje = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaPesaje = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EdadAnios = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EdadMeses = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EdadDias = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Peso = new System.Windows.Forms.DataGridViewLinkColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblRP
            // 
            this.lblRP.AutoSize = true;
            this.lblRP.Location = new System.Drawing.Point(55, 47);
            this.lblRP.Name = "lblRP";
            this.lblRP.Size = new System.Drawing.Size(25, 13);
            this.lblRP.TabIndex = 0;
            this.lblRP.Text = "RP:";
            // 
            // lblDesde
            // 
            this.lblDesde.AutoSize = true;
            this.lblDesde.Location = new System.Drawing.Point(55, 86);
            this.lblDesde.Name = "lblDesde";
            this.lblDesde.Size = new System.Drawing.Size(41, 13);
            this.lblDesde.TabIndex = 2;
            this.lblDesde.Text = "Desde:";
            // 
            // lblAniohasta
            // 
            this.lblAniohasta.AutoSize = true;
            this.lblAniohasta.Location = new System.Drawing.Point(394, 86);
            this.lblAniohasta.Name = "lblAniohasta";
            this.lblAniohasta.Size = new System.Drawing.Size(38, 13);
            this.lblAniohasta.TabIndex = 3;
            this.lblAniohasta.Text = "Hasta:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Seleccionar,
            this.IDPesaje,
            this.FechaPesaje,
            this.EdadAnios,
            this.EdadMeses,
            this.EdadDias,
            this.Peso});
            this.dataGridView1.Location = new System.Drawing.Point(94, 135);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(643, 150);
            this.dataGridView1.TabIndex = 4;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // txtRP
            // 
            this.txtRP.Location = new System.Drawing.Point(121, 44);
            this.txtRP.Name = "txtRP";
            this.txtRP.ReadOnly = true;
            this.txtRP.Size = new System.Drawing.Size(100, 20);
            this.txtRP.TabIndex = 5;
            // 
            // dateDesde
            // 
            this.dateDesde.Location = new System.Drawing.Point(121, 80);
            this.dateDesde.Name = "dateDesde";
            this.dateDesde.Size = new System.Drawing.Size(200, 20);
            this.dateDesde.TabIndex = 7;
            // 
            // dateHasta
            // 
            this.dateHasta.Location = new System.Drawing.Point(457, 80);
            this.dateHasta.Name = "dateHasta";
            this.dateHasta.Size = new System.Drawing.Size(200, 20);
            this.dateHasta.TabIndex = 8;
            // 
            // lblEliminar
            // 
            this.lblEliminar.AutoSize = true;
            this.lblEliminar.Location = new System.Drawing.Point(91, 288);
            this.lblEliminar.Name = "lblEliminar";
            this.lblEliminar.Size = new System.Drawing.Size(55, 13);
            this.lblEliminar.TabIndex = 9;
            this.lblEliminar.TabStop = true;
            this.lblEliminar.Text = "Eliminar (-)";
            this.lblEliminar.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblEliminar_LinkClicked);
            // 
            // lblNuevo
            // 
            this.lblNuevo.AutoSize = true;
            this.lblNuevo.Location = new System.Drawing.Point(686, 288);
            this.lblNuevo.Name = "lblNuevo";
            this.lblNuevo.Size = new System.Drawing.Size(54, 13);
            this.lblNuevo.TabIndex = 10;
            this.lblNuevo.TabStop = true;
            this.lblNuevo.Text = "Nuevo (+)";
            this.lblNuevo.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblNuevo_LinkClicked);
            // 
            // btnVolver
            // 
            this.btnVolver.Location = new System.Drawing.Point(365, 306);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(153, 34);
            this.btnVolver.TabIndex = 11;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // btnMostrar
            // 
            this.btnMostrar.Location = new System.Drawing.Point(689, 81);
            this.btnMostrar.Name = "btnMostrar";
            this.btnMostrar.Size = new System.Drawing.Size(75, 23);
            this.btnMostrar.TabIndex = 12;
            this.btnMostrar.Text = "Mostrar";
            this.btnMostrar.UseVisualStyleBackColor = true;
            this.btnMostrar.Click += new System.EventHandler(this.btnMostrar_Click);
            // 
            // Seleccionar
            // 
            this.Seleccionar.HeaderText = "Seleccionar";
            this.Seleccionar.Name = "Seleccionar";
            this.Seleccionar.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Seleccionar.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // IDPesaje
            // 
            this.IDPesaje.HeaderText = "Código";
            this.IDPesaje.Name = "IDPesaje";
            this.IDPesaje.ReadOnly = true;
            this.IDPesaje.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IDPesaje.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.IDPesaje.Visible = false;
            // 
            // FechaPesaje
            // 
            this.FechaPesaje.HeaderText = "Fecha Pesaje";
            this.FechaPesaje.Name = "FechaPesaje";
            this.FechaPesaje.ReadOnly = true;
            // 
            // EdadAnios
            // 
            this.EdadAnios.HeaderText = "Edad Años";
            this.EdadAnios.Name = "EdadAnios";
            this.EdadAnios.ReadOnly = true;
            // 
            // EdadMeses
            // 
            this.EdadMeses.HeaderText = "Edad Meses";
            this.EdadMeses.Name = "EdadMeses";
            this.EdadMeses.ReadOnly = true;
            // 
            // EdadDias
            // 
            this.EdadDias.HeaderText = "Edad Días";
            this.EdadDias.Name = "EdadDias";
            this.EdadDias.ReadOnly = true;
            // 
            // Peso
            // 
            this.Peso.HeaderText = "Peso";
            this.Peso.Name = "Peso";
            this.Peso.ReadOnly = true;
            this.Peso.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Peso.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ConsultaInfoPesajeporAnimal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(826, 370);
            this.Controls.Add(this.btnMostrar);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.lblNuevo);
            this.Controls.Add(this.lblEliminar);
            this.Controls.Add(this.dateHasta);
            this.Controls.Add(this.dateDesde);
            this.Controls.Add(this.txtRP);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.lblAniohasta);
            this.Controls.Add(this.lblDesde);
            this.Controls.Add(this.lblRP);
            this.Name = "ConsultaInfoPesajeporAnimal";
            this.Text = "Consulta de Información Pesaje por Animal";
            this.Load += new System.EventHandler(this.ConsultaInfoPesajeporAnimal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRP;
        private System.Windows.Forms.Label lblDesde;
        private System.Windows.Forms.Label lblAniohasta;
        public System.Windows.Forms.TextBox txtRP;
        private System.Windows.Forms.DateTimePicker dateDesde;
        private System.Windows.Forms.DateTimePicker dateHasta;
        private System.Windows.Forms.LinkLabel lblEliminar;
        private System.Windows.Forms.LinkLabel lblNuevo;
        private System.Windows.Forms.Button btnVolver;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnMostrar;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Seleccionar;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDPesaje;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaPesaje;
        private System.Windows.Forms.DataGridViewTextBoxColumn EdadAnios;
        private System.Windows.Forms.DataGridViewTextBoxColumn EdadMeses;
        private System.Windows.Forms.DataGridViewTextBoxColumn EdadDias;
        private System.Windows.Forms.DataGridViewLinkColumn Peso;
    }
}