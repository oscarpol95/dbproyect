﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;

namespace WindowsFormsApplication1
{
    class gastosAcciones
    {
        public static int EliminarGastos(string id_Gastos)
        {
            int retorno = 0;

            MySqlCommand cmd = new MySqlCommand("borrar_gastos", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("idGastos", id_Gastos));
            retorno = cmd.ExecuteNonQuery();
            coneccionBase.ObtenerConexion().Close();

            return retorno;
        }

        public static int EliminarDesgloseGastos(string id_desgloGastos)
        {
            int retorno = 0;

            MySqlCommand cmd = new MySqlCommand("borrar_desglose_gastos", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("id_desgloGastos", id_desgloGastos));
            retorno = cmd.ExecuteNonQuery();
            coneccionBase.ObtenerConexion().Close();

            return retorno;
        }
    }
}
