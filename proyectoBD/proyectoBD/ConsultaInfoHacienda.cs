﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace WindowsFormsApplication1
{
    public partial class ConsultaInfoHacienda : Form
    {
        public ConsultaInfoHacienda()
        {
            InitializeComponent();
        }

        private void lnkEditar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            IngresoDatosHacienda datos = new IngresoDatosHacienda();
            datos.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            MySqlCommand cmd = new MySqlCommand("buscar_hacienda", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;


            datos.Show();
            MySqlDataReader reader = cmd.ExecuteReader();
            datos.txtID.Visible = false;
            datos.label5.Visible = false;
            List<String> lista = new List<String>();
            while (reader.Read())
            {

                lista.Add(reader.GetString(0));

            }
            if (lista.Count == 0)
            {

                datos.btnguardar.Visible = true;
                datos.btnEditar.Visible = false;
            }
            else
            {

                datos.btnguardar.Visible = false;
                datos.btnEditar.Visible = true;
                datos.txtruc.Text = reader.GetString(0);
                datos.txtnombre.Text = reader.GetString(1);
                datos.txtcipropietario.Text = reader.GetString(2);
                datos.txtemail.Text = reader.GetString(3);
                datos.txtID.Text = reader.GetString(4);

            }
            this.Hide();
        }

        private void lnkNuevoArea_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            IngresoInfoArea area = new IngresoInfoArea(false);
            area.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            area.Show();
            this.Hide();
        }

        private void lnkNuevoPersonal_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            WindowsFormsApplication1.IngresoPersonas personal = new WindowsFormsApplication1.IngresoPersonas(false);
            personal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            personal.Show();
            this.Hide();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void otherForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            dvgArea.Rows.Clear();
            MySqlCommand cmd = new MySqlCommand("buscar_area", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
                dvgArea.Rows.Add(false, reader.GetString(1), reader.GetString(3), reader.GetString(4), reader.GetString(2));

            dgvPersona.Rows.Clear();
            MySqlCommand cmd1 = new MySqlCommand("buscar_persona", coneccionBase.ObtenerConexion());
            cmd1.CommandType = CommandType.StoredProcedure;
            MySqlDataReader reader1 = cmd1.ExecuteReader();
            while (reader1.Read())
                dgvPersona.Rows.Add(false, reader1.GetString(0), reader1.GetString(1), reader1.GetString(2), reader1.GetString(4), reader1.GetString(3));


            MySqlCommand cmd2;

            try
            {
                cmd2 = new MySqlCommand("buscar_hacienda", coneccionBase.ObtenerConexion());
                cmd2.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.Add(new MySqlParameter("ruc", "1304014382001"));

                MySqlDataReader reader2 = cmd2.ExecuteReader();
                reader2.Read();
                this.txtRUC.Text = reader2.GetString(0);
                this.txtCI.Text = reader2.GetString(1);
                this.txtNombre.Text = reader2.GetString(2);
                this.txtEmail.Text = reader2.GetString(3);
            }
            catch (Exception exc)
            {
                Console.WriteLine("{0} First exception caught.", exc);
            }


            this.Show();
        }

        private void dvgArea_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1 && e.RowIndex >=0)
            {
                IngresoInfoArea area = new IngresoInfoArea(true);
                
                string celda= dvgArea.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();

                MySqlCommand cmd = new MySqlCommand("buscar_areaNombre", coneccionBase.ObtenerConexion());
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("nom",celda));
                
                area.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                area.Show();
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();

               
                area.txtNombre.Text = reader.GetString(1);
                area.txtDireccion.Text= reader.GetString(2);
                area.txtDimensiones.Text = reader.GetString(3);
                area.txtCapacidadMax.Text = reader.GetString(4);
                area.txtCodigo.Text = reader.GetString(0);

                
                this.Hide();
            }
        }

        private void dgvPersona_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2 && e.RowIndex >= 0)
            {
                WindowsFormsApplication1.IngresoPersonas persona = new WindowsFormsApplication1.IngresoPersonas(true);
                string celda = dgvPersona.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();

                MySqlCommand cmd = new MySqlCommand("buscar_personNombre", coneccionBase.ObtenerConexion());
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("nom",celda));

                persona.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                persona.Show();
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();

                persona.txtcedula.Text= reader.GetString(0);
                persona.txtnombre.Text = reader.GetString(1);
                persona.txtdireccion.Text = reader.GetString(3);
                persona.txtconvencional.Text = reader.GetString(4);
                persona.txtmovil.Text = reader.GetString(5);


                this.Hide();
            }
        }

        private void ConsultaInfoHacienda_Load(object sender, EventArgs e)
        {
            MySqlCommand cmd = new MySqlCommand("buscar_area", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
                dvgArea.Rows.Add(true,reader.GetString(1), reader.GetString(3), reader.GetString(4), reader.GetString(2));

            MySqlCommand cmd1 = new MySqlCommand("buscar_persona", coneccionBase.ObtenerConexion());
            cmd1.CommandType = CommandType.StoredProcedure;
            MySqlDataReader reader1 = cmd1.ExecuteReader();
            while (reader1.Read())
                dgvPersona.Rows.Add(true, reader1.GetString(0), reader1.GetString(1), reader1.GetString(2), reader1.GetString(4), reader1.GetString(3));
        }

        private void lnkEliminarArea_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            List<DataGridViewRow> toDelete = new List<DataGridViewRow>();
            foreach (DataGridViewRow row in dvgArea.Rows)
            {
                if (Convert.ToBoolean(row.Cells[0].Value) == true)
                {
                    toDelete.Add(row);
                }
            }
            if (!toDelete.Any())
                MessageBox.Show("Debe seleccionar una fila");
            else
            {
                foreach (DataGridViewRow row in toDelete)
                {
                    string nom = Convert.ToString(row.Cells[1].Value);
                    if (!tieneAnimales(nom))
                    {
                        areaAcciones.Eliminar(nom);
                        dvgArea.Rows.Remove(row);
                    }
                    else MessageBox.Show("No se puede eliminar el area " + nom + " porque tiene animales.");
                }
            }
        }

        private bool tieneAnimales(string area)
        {
            int conteo = 0;
            MySqlCommand cmd = new MySqlCommand("conteoAnimalesArea", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@inArea", area);
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
                conteo = reader.GetInt16(0);
            if (conteo > 0) return true;
            return false;
        }



        private void lnkEliminarPersonal_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            List<DataGridViewRow> toDelete = new List<DataGridViewRow>();
            foreach (DataGridViewRow row in dgvPersona.Rows)
            {
                if (Convert.ToBoolean(row.Cells[0].Value) == true)
                {
                    toDelete.Add(row);
                }
            }
            if (!toDelete.Any())
                MessageBox.Show("Debe seleccionar una fila");
            else
            {
                foreach (DataGridViewRow row in toDelete)
                {
                    string ced = Convert.ToString(row.Cells[1].Value);
                    if (!tieneAnimalesPersona(ced))
                    {
                        personaAcciones.Eliminar(ced);
                        dgvPersona.Rows.Remove(row);
                    }
                    else MessageBox.Show("No se puede eliminar la persona " + Convert.ToString(row.Cells[2].Value) + " porque tiene animales.");
                }
            }
        }

        private bool tieneAnimalesPersona(string persona)
        {
            int conteo = 0;
            MySqlCommand cmd = new MySqlCommand("conteoAnimalesPersona", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@inPersona", persona);
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
                conteo = reader.GetInt16(0);
            if (conteo > 0) return true;
            return false;
        }

        private void dvgArea_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1 && e.RowIndex >= 0)
            {
                IngresoInfoArea area = new IngresoInfoArea(true);

                string celda = dvgArea.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();

                MySqlCommand cmd = new MySqlCommand("buscar_areaNombre", coneccionBase.ObtenerConexion());
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("nom", celda));

                area.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                area.Show();
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                area.txtNombre.Text = reader.GetString(1);
                area.txtDireccion.Text = reader.GetString(2);
                area.txtDimensiones.Text = reader.GetString(3);
                area.txtCapacidadMax.Text = reader.GetString(4);
                area.txtCodigo.Text = reader.GetString(0);
                this.Hide();
            }
        }

        private void dgvPersona_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2 && e.RowIndex >= 0)
            {
                WindowsFormsApplication1.IngresoPersonas persona = new WindowsFormsApplication1.IngresoPersonas(true);
                string celda = dgvPersona.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();

                MySqlCommand cmd = new MySqlCommand("buscar_personNombre", coneccionBase.ObtenerConexion());
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("nom", celda));

                persona.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                persona.Show();
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                persona.txtcedula.Text = reader.GetString(0);
                persona.txtnombre.Text = reader.GetString(1);
                persona.txtdireccion.Text = reader.GetString(2);
                persona.txtconvencional.Text = reader.GetString(3);
                persona.txtmovil.Text = reader.GetString(4);
                persona.txtId.Text = reader.GetString(5);

                this.Hide();
            }
        }


        private IngresoDatosHacienda datos;
        private void GetOtherFormTextBox()
        {

            this.txtRUC.Text = datos.txtruc.Text;
            this.txtNombre.Text = datos.txtnombre.Text;
            this.txtCI.Text = datos.txtcipropietario.Text;
            this.txtEmail.Text = datos.txtemail.Text;

        }

        private void lblDatosGenerales_Click(object sender, EventArgs e)
        {

        }

        private void txtRUC_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
}
