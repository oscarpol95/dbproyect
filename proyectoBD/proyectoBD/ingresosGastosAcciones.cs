﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.Data;
namespace WindowsFormsApplication1
{
    class ingresosGastosAcciones
    {
        public static int AgregarGasto(ingresosGastos gastos)
        {

            int retorno = 0;
            MySqlCommand cmd = new MySqlCommand("new_ingreso_gastos", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new MySqlParameter("idgastos", null));
            cmd.Parameters.Add(new MySqlParameter("fecha", gastos.fecha));
            cmd.Parameters.Add(new MySqlParameter("tipo_rubro", gastos.tipo_rubro));
            cmd.Parameters.Add(new MySqlParameter("detalle", gastos.detalle));
            cmd.Parameters.Add(new MySqlParameter("cantidad", gastos.cantidad));
            cmd.Parameters.Add(new MySqlParameter("costo_uni", gastos.costo_uni));
            cmd.Parameters.Add(new MySqlParameter("cargos_adi", gastos.cargos_adi));
            cmd.Parameters.Add(new MySqlParameter("descuento", gastos.descuento));
            cmd.Parameters.Add(new MySqlParameter("cedula", gastos.cedula));

            retorno = cmd.ExecuteNonQuery();
            return retorno;
        }

        public static int AgregarGastoPersonal(ingresosGastos gastos)
        {

            int retorno = 0;
            MySqlCommand cmd = new MySqlCommand("new_gastoPersonal", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            
            cmd.Parameters.Add(new MySqlParameter("cedula", gastos.cedula));
            cmd.Parameters.Add(new MySqlParameter("idgastos", null));
           
            retorno = cmd.ExecuteNonQuery();
            return retorno;
        }

        public static int ActualizarGasto(ingresosGastos gastos)
        {

            int retorno = 0;
            MySqlCommand cmd = new MySqlCommand("update_gasto", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new MySqlParameter("idgastos", gastos.idgastos));
            cmd.Parameters.Add(new MySqlParameter("fecha", gastos.fecha));
            cmd.Parameters.Add(new MySqlParameter("tipo_rubro", gastos.tipo_rubro));
            cmd.Parameters.Add(new MySqlParameter("detalle", gastos.detalle));
            cmd.Parameters.Add(new MySqlParameter("cantidad", gastos.cantidad));
            cmd.Parameters.Add(new MySqlParameter("costo_uni", gastos.costo_uni));
            cmd.Parameters.Add(new MySqlParameter("cargos_adi", gastos.cargos_adi));
            cmd.Parameters.Add(new MySqlParameter("descuento", gastos.descuento));
            cmd.Parameters.Add(new MySqlParameter("cedula", gastos.cedula));

            retorno = cmd.ExecuteNonQuery();
            return retorno;
        }
    }
}
