﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    public partial class ConsultaInfoPesajeporAnimal : Form
    {
        public ConsultaInfoPesajeporAnimal()
        {
            InitializeComponent();
        }

        private void ConsultaInfoPesajeporAnimal_Load(object sender, EventArgs e)
        {
            MySqlCommand cmd = new MySqlCommand("buscar_pesaje", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("rp", txtRP.Text));

            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
                dataGridView1.Rows.Add(false, reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5));

        }

        private void lblNuevo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            IngresoPesajes pesaje = new IngresoPesajes(false);
            pesaje.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            pesaje.txtRP.Text = this.txtRP.Text;
            pesaje.Show();
            this.Hide();
        }

        void otherForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            dataGridView1.Rows.Clear();
            MySqlCommand cmd = new MySqlCommand("buscar_pesaje", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("rp", txtRP.Text));

            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
                dataGridView1.Rows.Add(false, reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5));

            this.Show();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6 && e.RowIndex >= 0)
            {
                IngresoPesajes animal = new IngresoPesajes(true);
                animal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                animal.txtRP.Text = this.txtRP.Text;
                animal.txtPeso.Text = Convert.ToString(dataGridView1.CurrentRow.Cells[6].Value);
                animal.datePesaje.Text= Convert.ToString(dataGridView1.CurrentRow.Cells[2].Value);
                animal.txtID.Text = Convert.ToString(dataGridView1.CurrentRow.Cells[1].Value);
                animal.Show();
                this.Hide();
            }
        }

        private void lblEliminar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            List<DataGridViewRow> toDelete = new List<DataGridViewRow>();
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (Convert.ToBoolean(row.Cells[0].Value) == true)
                {
                    toDelete.Add(row);
                }
            }
            if (!toDelete.Any())
                MessageBox.Show("Debe seleccionar una fila");
            else
            {
                foreach (DataGridViewRow row in toDelete)
                {
                    string nom = Convert.ToString(row.Cells[1].Value);
                    pesajeAcciones.Eliminar(nom);
                    dataGridView1.Rows.Remove(row);
                }
            }
        }

        private void btnMostrar_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            MySqlCommand cmd = new MySqlCommand("buscar_pesajeFecha", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("rp", txtRP.Text));
            string fechaDesde = dateDesde.Value.Year + "/" + dateDesde.Value.Month + "/" + dateDesde.Value.Day;
            string fechaHasta = dateHasta.Value.Year + "/" + dateHasta.Value.Month + "/" + dateHasta.Value.Day;
            cmd.Parameters.Add(new MySqlParameter("fecha_desde", fechaDesde));
            cmd.Parameters.Add(new MySqlParameter("fecha_hasta", fechaHasta));
            MySqlDataReader reader = cmd.ExecuteReader();


            while (reader.Read())
                dataGridView1.Rows.Add(false, reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5));

        }
    }
}
