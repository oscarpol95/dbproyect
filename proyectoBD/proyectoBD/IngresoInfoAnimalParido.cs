﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace WindowsFormsApplication1
{
    public partial class IngresoInfoAnimalParido : Form
    {
        public IngresoInfoAnimalParido()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtRP.Text)||string.IsNullOrWhiteSpace(cmbEstado.Text)||string.IsNullOrWhiteSpace(cmbRaza.Text)
                ||string.IsNullOrWhiteSpace(cmbPropietario.Text)||string.IsNullOrWhiteSpace(txtPadre.Text)
                ||string.IsNullOrWhiteSpace(txtMadre.Text)/*||string.IsNullOrWhiteSpace(txtRGDPadre.Text)||string.IsNullOrWhiteSpace(txtRGD.Text)*/)

                MessageBox.Show("Campos Obligatorios Vacios!", "Campos Vacios!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else if (txtRP.Text.Trim(' ').Length < 0  || txtMadre.Text.Trim(' ').Length < 0  || txtPadre.Text.Trim(' ').Length < 0)
            {
                MessageBox.Show("Faltan Caracteres en el campo!", "Faltan Caracteres en el Campo RP (deben ser 3 caracteres)!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }


            else

            {
                parto parto = new parto();
                parto.rp_padre = txtPadre.Text.Trim();
                parto.fecha_parto = txtFechaNacimiento.Text.Trim();
                parto.sexo = txtSexo.Text.Trim();
                parto.estado = "Vivo";
                parto.rp_vaca = txtMadre.Text.Trim();

                animal animal = new animal();
                animal.rp = txtRP.Text.Trim();
                animal.n_arete = txtArete.Text.Trim();
                animal.id_area = cmbUbicacion.Text.Trim();
                animal.nombre = txtNombre.Text.Trim();
                animal.sexo = txtSexo.Text.Trim();
                animal.propi = cmbPropietario.Text.Trim();
                animal.rp_madre = txtMadre.Text.Trim();
                animal.rp_padre = txtPadre.Text.Trim();
                animal.estado = cmbEstado.Text.Trim();
                animal.raza = cmbRaza.Text.Trim();

                edades edad = new edades();
                edad.rp = txtRP.Text.Trim();
                edad.fecha_naci = txtFechaNacimiento.Text.Trim();

                int resultado = partoAcciones.Agregar(parto);
                int resultado2 = animalAcciones.Agregar(animal);
                int resultado3 = edadesAcciones.Agregar(edad);
                if (resultado>0 && resultado2 > 0 && resultado3>0)
                {
                    MessageBox.Show("Animal Guardado Con Exito!!", "Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();

                }
                else
                {
                    MessageBox.Show("No se pudo guardar el animal", "Fallo!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
        }

        private static void SoloNumeros(KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }


        private void txtRP_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar) && e.KeyChar != '\b')
            {
                e.Handled = true;
                MessageBox.Show("Solo se permiten letras", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void txtArete_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void txtRDG_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void txtPadre_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void txtMadre_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void txtRGDPadre_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void btnDescartar_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Si prosigue se eliminará el parto que ingreso. ¿Desea seguir? ", "Alerta", MessageBoxButtons.YesNo);
            if(dr == DialogResult.Yes) {
                this.Close();
            }
        }

        private void IngresoInfoAnimalParido_Load(object sender, EventArgs e)
        {
            List<String> _lista = new List<String>();
            MySqlCommand cmd = new MySqlCommand("buscar_persona", coneccionBase.ObtenerConexion());
            cmd.CommandType = CommandType.StoredProcedure;
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                _lista.Add(reader.GetString(1));
            }
            cmbPropietario.DataSource = _lista;

            List<String> _lista2 = new List<String>();
            MySqlCommand cmd2 = new MySqlCommand("buscar_area", coneccionBase.ObtenerConexion());
            cmd2.CommandType = CommandType.StoredProcedure;
            MySqlDataReader reader2 = cmd2.ExecuteReader();
            while (reader2.Read())
            {
                _lista2.Add(reader2.GetString(1));
            }
            cmbUbicacion.DataSource = _lista2;
        }

        private void txtRP_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblNoArete_Click(object sender, EventArgs e)
        {

        }

        private void cmbUbicacion_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
