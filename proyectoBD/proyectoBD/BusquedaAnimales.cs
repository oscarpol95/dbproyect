﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace WindowsFormsApplication1
{
    public partial class BusquedaAnimales : Form
    {
        public BusquedaAnimales()
        {
            InitializeComponent();
        }

        private void lnkNuevo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            WindowsFormsApplication1.IngresoAnimalNuevo animal = new WindowsFormsApplication1.IngresoAnimalNuevo();
            animal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            animal.Show();
            this.Hide();
        }

        private void btnvolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void otherForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            dgvbusquedaanimal.Rows.Clear();
            txtcriterio.Text = "";
            BusquedaAnimales_Load(sender, e);
            this.Show();
        }

        private void dgvbusquedaanimal_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.RowIndex >= 0) {
                string rp_click = dgvbusquedaanimal.Rows[e.RowIndex].Cells[1].Value.ToString();
                string cont_celda = dgvbusquedaanimal.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();

                if (e.ColumnIndex == 1)//RP ANIMAL
                {
                    WindowsFormsApplication1.ConsultaInfoAnimal animal = new WindowsFormsApplication1.ConsultaInfoAnimal();
                    animal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);

                    animal.txtRP.Text = rp_click;
                    animal.txtUbicacion.Text = dgvbusquedaanimal.Rows[e.RowIndex].Cells[2].Value.ToString();
                    animal.txtNombre.Text = dgvbusquedaanimal.Rows[e.RowIndex].Cells[4].Value.ToString();
                    animal.txtSexo.Text = dgvbusquedaanimal.Rows[e.RowIndex].Cells[5].Value.ToString();
                    animal.txtEstado.Text = dgvbusquedaanimal.Rows[e.RowIndex].Cells[8].Value.ToString();
                    animal.txtPropietario.Text = dgvbusquedaanimal.Rows[e.RowIndex].Cells[3].Value.ToString();
                    animal.txtFechaNacimiento.Text = dgvbusquedaanimal.Rows[e.RowIndex].Cells[6].Value.ToString();

                    MySqlCommand cmd2;

                    try
                    {
                        cmd2 = new MySqlCommand("buscar_animal_info", coneccionBase.ObtenerConexion());
                        cmd2.CommandType = CommandType.StoredProcedure;
                        cmd2.Parameters.Add(new MySqlParameter("RP_in", rp_click));

                        MySqlDataReader reader2 = cmd2.ExecuteReader();
                        reader2.Read();
                        animal.txtArete.Text = reader2.GetString(0);
                        animal.txtRGD.Text = reader2.GetString(1);
                        animal.txtRaza.Text = reader2.GetString(2);
                        animal.txtClasificacion.Text = reader2.GetString(3);
                        animal.txtEdad.Text = reader2.GetString(6) + " años, " + reader2.GetString(7) + " meses";
                        animal.txtHaciendaOrigen.Text = reader2.GetString(5);
                        animal.txtFechaLlegada.Text = reader2.GetString(4);
                        

                        
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine("{0} First exception caught.", exc);
                    }

                    animal.dataGridView1.Rows.Clear();
                    MySqlCommand cmd = new MySqlCommand("buscar_genealogia", coneccionBase.ObtenerConexion());
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new MySqlParameter("RP_in", rp_click));

                    MySqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                        animal.dataGridView1.Rows.Add(reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4));

                    animal.Show();
                    this.Hide();
                }
                else if (e.ColumnIndex == 7)
                {
                    if (cont_celda != "No Aplica")
                    {
                        WindowsFormsApplication1.ConsInfoParto animal = new WindowsFormsApplication1.ConsInfoParto();
                        animal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                        animal.txtRP.Text = rp_click;
                        animal.Show();
                        this.Hide();
                    }

                }

                else if (e.ColumnIndex == 8)
                {
                    if (cont_celda != "No Aplica")
                    {
                        InformacionPlanesReproductivoPorVaca animal = new InformacionPlanesReproductivoPorVaca();
                        animal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                        animal.txtrp.Text = rp_click;
                        MySqlCommand cmd = new MySqlCommand("update_control_gestacion", coneccionBase.ObtenerConexion());
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                        animal.Show();
                        this.Hide();
                    }

                }
                else if (e.ColumnIndex == 9)
                {

                    WindowsFormsApplication1.ConsultaInfoPesajeporAnimal animal = new WindowsFormsApplication1.ConsultaInfoPesajeporAnimal();
                    animal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                    animal.txtRP.Text = rp_click;
                    MySqlCommand cmd = new MySqlCommand("update_edades", coneccionBase.ObtenerConexion());
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                    animal.Show();
                    this.Hide();


                }
                else if (e.ColumnIndex == 10)
                {

                    WindowsFormsApplication1.ConsultaInfoPlanSanitario animal = new WindowsFormsApplication1.ConsultaInfoPlanSanitario();
                    animal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                    animal.txtRP.Text = rp_click;
                    
                    animal.Show();
                    this.Hide();

                } 
            }           
        }
        public void bloquea_NoAplica(DataGridView dgv){
        //está función es cuando el animal es macho
            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                for (int k = 0; k < dgv.Columns.Count; k++)
                {
                    if (dgv[k,i].Value.ToString() == "No Aplica")
                    {
                       
                        dgv.Rows[i].Cells[k] = new DataGridViewTextBoxCell();

                        dgv.Rows[i].Cells[k].Value = "No Aplica";
                        dgv.Rows[i].Cells[k].Selected = false;
                        
                    }
                }
            }
        }
        private void BusquedaAnimales_Load(object sender, EventArgs e)
        {
            MySqlCommand cmd = new MySqlCommand("buscar_animal", coneccionBase.ObtenerConexion());
             cmd.CommandType = CommandType.StoredProcedure;
             MySqlDataReader reader = cmd.ExecuteReader();
             while (reader.Read())
                dgvbusquedaanimal.Rows.Add(false, reader.GetString(0),reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4),reader.GetString(5), reader.GetString(6),reader.GetString(7), reader.GetString(8), reader.GetString(9));
            bloquea_NoAplica(dgvbusquedaanimal);
        }

        private void btnbuscar_Click(object sender, EventArgs e)
        {
            
            dgvbusquedaanimal.Rows.Clear();
            string criterio = txtcriterio.Text.Trim();
            MySqlCommand cmd = null;

            if (criterio == "")
            {
                cmd = new MySqlCommand("buscar_animal", coneccionBase.ObtenerConexion());
            }
            else if (rdrp.Checked == true)
            {
                using (cmd = new MySqlCommand("buscar_animal_rp", coneccionBase.ObtenerConexion()))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@clave", criterio);
                }
            }
            else if (rdnombre.Checked == true)
            {
                using (cmd = new MySqlCommand("buscar_animal_nombre", coneccionBase.ObtenerConexion()))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@clave", criterio);
                }
            }
            else if (rdpropietario.Checked == true)
            {
                using (cmd = new MySqlCommand("buscar_animal_propietario", coneccionBase.ObtenerConexion()))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@clave", criterio);
                }
            }
            else if (rdubicacion.Checked == true)
            {
                using (cmd = new MySqlCommand("buscar_animal_area", coneccionBase.ObtenerConexion()))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@clave", criterio);
                }
            }
            else
            {
                cmd = new MySqlCommand("buscar_animal", coneccionBase.ObtenerConexion());

            }
            cmd.CommandType = CommandType.StoredProcedure;

            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
             
                dgvbusquedaanimal.Rows.Add(false, reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5), reader.GetString(6),reader.GetString(7), reader.GetString(8), reader.GetString(9));
               

            }
            bloquea_NoAplica(dgvbusquedaanimal);
           
        }

        private void lnkEliminar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            List<DataGridViewRow> toDelete = new List<DataGridViewRow>();
            foreach (DataGridViewRow row in dgvbusquedaanimal.Rows)
            {
                if (Convert.ToBoolean(row.Cells[0].Value) == true)
                {
                    toDelete.Add(row);
                }
            }
            if (!toDelete.Any())
                MessageBox.Show("Debe seleccionar una fila");
            else
            {
                foreach (DataGridViewRow row in toDelete)
                {
                    string rp = Convert.ToString(row.Cells[1].Value);
                    animalAcciones.Eliminar(rp);
                    dgvbusquedaanimal.Rows.Remove(row);
                }
            }
        }
    }
}
