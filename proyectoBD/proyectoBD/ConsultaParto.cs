﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class ConsInfoParto : Form
    {
        public ConsInfoParto()
        {
            InitializeComponent();
    
        }

        private void ConsInfoParto_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lblSelec_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3 && e.RowIndex >= 0)
            {
                IngresoParto animal = new IngresoParto();
                animal.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
                animal.txtRPMadre.Text = this.txtRP.Text;
                animal.Show();
                this.Hide();
            }
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblNuevo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            IngresoParto parto = new IngresoParto();
            parto.FormClosed += new FormClosedEventHandler(otherForm_FormClosed);
            parto.txtRPMadre.Text = this.txtRP.Text;
            parto.Show();
            this.Hide();
        }

        void otherForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
        }

        private void lblEliminar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            List<DataGridViewRow> toDelete = new List<DataGridViewRow>();
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (Convert.ToBoolean(row.Cells[0].Value) == true)
                {
                    toDelete.Add(row);
                }
            }
            if (!toDelete.Any())
                MessageBox.Show("Debe seleccionar una fila");
            else
            {
                foreach (DataGridViewRow row in toDelete)
                {
                    string nom = Convert.ToString(row.Cells[1].Value);
                    partoAcciones.Eliminar(nom);
                    dataGridView1.Rows.Remove(row);
                }
            }
        }

        private void btnMostrar_Click(object sender, EventArgs e)
        {

        }
    }
}
