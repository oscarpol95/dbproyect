USE  discosdurostest;

DELIMITER @@
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_bajas_propietario`(IN propietario VARCHAR(50))
BEGIN
Select rp_vaca, fecha_baja,tipo_baja, peso_libras, precio_por_libras  FROM bajas, animal where propietario=animales.propietario AND animales.RP=rp_vaca;

END@@
DELIMITER ;

DELIMITER @@
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_bajas_tipo`(IN tipo VARCHAR(50))
BEGIN
Select rp_vaca, fecha_baja,tipo_baja, peso_libras, precio_por_libras  FROM bajas where tipo_baja=tipo
order by tipo desc;
END@@
DELIMITER ;

DELIMITER @@
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_bajas_nombre`(IN nombre VARCHAR(50))
BEGIN
Select rp_vaca, fecha_baja,tipo_baja, peso_libras, precio_por_libras FROM bajas, animal where nombre=animal.nombre and rp_vaca=animal.RP
order by rp desc;
END@@
DELIMITER ;

DELIMITER @@
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscar_bajasFecha`(IN fecha_desde DATE,IN fecha_hasta DATE)
BEGIN
select rp_vaca, fecha_baja, tipo_baja, peso_libras, precio_por_libras
from bajas, edades
where (bajas.fecha_baja between fecha_desde  and fecha_hasta);
END@@
DELIMITER ;